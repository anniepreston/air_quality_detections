export default `\
#define SHADER_NAME satellite-detection-layer-vertex-shader

attribute vec2 satellitePositions;
attribute vec3 satelliteValues;
attribute vec2 interpPositions;
attribute vec4 interpValues;
varying float opacity;
uniform vec4 minColor;
uniform vec4 maxColor;
uniform vec3 cellScale;
varying vec2 texCoord;
uniform float interpolation_on;
uniform float frp_color;
varying float interp_on;

void main(void) {
    if (interpolation_on != 1.0){
	    opacity = satelliteValues.z;
	}
	else { opacity = interpValues.w; }
	vec2 target_pos;
	
	if (interpolation_on != 1.0){
		if (frp_color == 1.0){
			texCoord = vec2(0.0,1.0-satelliteValues.x);
		}
		else {
			texCoord = vec2(1.0, 1.0-satelliteValues.z);
		}
		target_pos = project_position(satellitePositions).xy;
	}
	else { 
		if (frp_color == 1.0){
	    	texCoord = vec2(0.0, 1.0 - interpValues.x);
		}
		else { texCoord = vec2(1.0, 1.0 - interpValues.z); }
	    //texCoord = vec2(0.0, 1.0-interpValues.x);
	    target_pos = project_position(interpPositions).xy;
	}
	
	interp_on = interpolation_on;
	
	if (interp_on != 1.0){
		gl_Position = project_to_clipspace(vec4(target_pos, 0.0, 1.0));
	}
	else {
		gl_Position = project_to_clipspace(vec4(target_pos, 0.0, 1.0));
	}
}

`;
