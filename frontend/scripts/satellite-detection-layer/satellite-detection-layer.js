import { Layer } from 'deck.gl';
import { GL, Model, Geometry, LoadTextures, Texture2D} from '@luma.gl/core';

import satelliteVertex from './satellite-detection-layer-vertex.glsl';
import satelliteFragment from './satellite-detection-layer-fragment.glsl';

//import { interpolatePoints, interpolateValues, getScaledFRP, getScaledConfidence } from '../fire-interpolate.js';

const defaultProps = {
    colors: {
		0.0: "#fff7bc",
		0.14: "#fee391",
		0.29: "#fec44f",
		0.43: "#fe9929",
		0.57: "#ec7014",
		0.71: "#cc4c02",
		0.86: "#993404",
		1.0: "#662506"
    },
 	greys: {
		0.0: "#ffffff",
		0.14: "#f0f0f0",
		0.29: "#d9d9d9",
		0.43: "#bdbdbd",
		0.57: "#969696",
		0.71: "#737373",
		0.86: "#525252",
		1.0: "#252525"
  	},
  	getPosition: d => d.position,
	getWeight: d => 1
};

export default class SatelliteDetectionLayer extends Layer {
	initializeState(){
		const {gl} = this.context;
		const attributeManager = this.getAttributeManager();
		const model = this.getModel(gl);

		//FIXME: need unified/intuitive way to set these parameters
		const interpolation_on = 1.0; // show raw detections (false), or interpolated detections (true)?
		const uncertainty_intrinsic = 1.0; // intrinsic: uncertainty mapped to grayness; extrinsic: using glyphs
		const uncertainty_coincident = 1.0; // coincident: in same image; adjacent: in neighboring panel/image

		const { colors, greys } = this.props;
		const colorData = this.getColorScale(colors, greys);

		if (interpolation_on == 0.0){
			attributeManager.add({
			    satellitePositions: { size: 2, update: this.calculatePositions },
			    satelliteValues: { size: 3, update: this.calculateValues },
		    });
		}
		
		else{
			attributeManager.add({
			    interpPositions: { size: 2, update: this.getInterpPositions },
			    interpValues: { size: 4, update: this.getInterpValues }
		    });
		}

		gl.getExtension('OES_element_index_unit');
		this.setState({model, interpolation_on, colorData});
	}
	
	updateState({props, changeFlags: {dataChanged}}) {
		if (dataChanged) {
			this.state.attributeManager.invalidateAll();
		}
	}
	
	draw({uniforms}) {
		const {mapping} = this.props;
		const {model, interpolation_on, colorData} = this.state;
		const frp_color = (mapping == 'color-frp' ? 1.0 : 0.0);
		const colorTexture = new Texture2D(this.context.gl, {
			data: colorData,
			width: 16,
			height: 16,
			format: this.context.gl.RGBA32F,
			type: this.context.gl.FLOAT,
			dataFormat: this.context.gl.RGBA,
			parameters: {
			  [this.context.gl.TEXTURE_MAG_FILTER]: this.context.gl.NEAREST,
			  [this.context.gl.TEXTURE_MIN_FILTER]: this.context.gl.NEAREST,
			  [this.context.gl.TEXTURE_WRAP_S]: this.context.gl.CLAMP_TO_EDGE,
			  [this.context.gl.TEXTURE_WRAP_T]: this.context.gl.CLAMP_TO_EDGE
		  	},
		  	pixelStore: {[this.context.gl.UNPACK_FLIP_Y_WEBGL]: true},
		  	mipmaps: true
		});

      	uniforms = Object.assign({}, uniforms, { 
      		colorTexture: colorTexture, 
      		interpolation_on: interpolation_on,
      		frp_color: frp_color
      	});

      	model.draw({
        	uniforms: uniforms
      	});
	}
	
	getModel(gl){
		return new Model(gl, {
			id: this.props.id,
			vs: satelliteVertex,
			fs: satelliteFragment,
			geometry: new Geometry({
				id: this.props.id,
				drawMode: 'TRIANGLE_STRIP'
			}),
			vertexCount: 0,
			isInstanced: false,
			shaderCache: this.context.shaderCache
		});
	}
	
	calculatePositions(attribute){
		// const {modis_data, goes_data, frp_data, clusters} = this.props;
		// //FIXME: incorporate all types	
		// /*	
  //   	const modis_polygons = modis_data.features.filter(function(item) { return item.geometry.type == 'Polygon'; }),
		// 	goes_polygons = goes_data.features.filter(function(item) {  return item.geometry.type == 'Polygon'; }),
		// 	modis_points = frp_data.features.filter(function(item) { return item.geometry.type == 'Point'; });
		// const polygons = goes_polygons.concat(modis_polygons);
		// */
		// const size = attribute.size,
	 //        value = attribute.value;  
	 //    /*
		// var rawCoords = [];
  //   	for (var i = 0; i < polygons.length; i++){
  //   		if (this.isInCalifornia(polygons[i].geometry.coordinates[0][0][0],polygons[i].geometry.coordinates[0][0][1])){
  //   			rawCoords.push(polygons[i].geometry.coordinates[0]);
  //   		}
  //   	}
  //   	*/
  //   	//TEMP!
  //   	//var rawCoords = frp_data;
    	
  //   	var valueCoords = new Float32Array(frp_data.length*6*2 - 2);// + clusters.bounds.length * 6 * 2); //FIXME
  //   	const idxs = [0, 1, 3, 2]; //FIXME: simplify this
  //   	for (var i = 0; i < frp_data.length; i++){
		// 	if (i != 0) {  // first index of next triangle (i.e. next in the last iteration)
		// 		valueCoords[(i - 1) * 6 * size + 5 * size + 0] = frp_data[i].footprint.coordinates[0][0][0];//rawCoords[i][0][0]; 
		// 		valueCoords[(i - 1) * 6 * size + 5 * size + 1] = frp_data[i].footprint.coordinates[0][0][1];//rawCoords[i][0][1];
		// 	}
		// 	for (var j = 0; j < 4; j++){ // 0, 1, 3, 2 
		// 		var k = idxs[j];
		// 		valueCoords[i * 6 * size + j * size + 0] = frp_data[i].footprint.coordinates[0][k][0];//rawCoords[i][k][0];
		// 		valueCoords[i * 6 * size + j * size + 1] = frp_data[i].footprint.coordinates[0][k][1];//rawCoords[i][k][1];
		// 	}
		// 	valueCoords[i * 6 * size + 4 * size + 0] = frp_data[i].footprint.coordinates[0][k][0];//rawCoords[i][k][0]; //last index of prev triangle
		// 	valueCoords[i * 6 * size + 4 * size + 1] = frp_data[i].footprint.coordinates[0][k][1];//rawCoords[i][k][1];
  //   	}
    	
  //   	/*
  //   	const clusterBounds = clusters.bounds;
  //   	const totalBounds = clusters.totalBounds;

  //   	for (var j = 0; j < clusterBounds.length; j++){
		// 	valueCoords[(i - 1) * 6 * size + 5 * size + 0] = clusterBounds[j].lonMin;
		// 	valueCoords[(i - 1) * 6 * size + 5 * size + 1] = clusterBounds[j].latMin;
		// 	valueCoords[i * 6 * size + 0 * size + 0] = clusterBounds[j].lonMin;
		// 	valueCoords[i * 6 * size + 0 * size + 1] = clusterBounds[j].latMin;
		// 	valueCoords[i * 6 * size + 1 * size + 0] = clusterBounds[j].lonMin;
		// 	valueCoords[i * 6 * size + 1 * size + 1] = clusterBounds[j].latMax;
		// 	valueCoords[i * 6 * size + 2 * size + 0] = clusterBounds[j].lonMax;
		// 	valueCoords[i * 6 * size + 2 * size + 1] = clusterBounds[j].latMin;
		// 	valueCoords[i * 6 * size + 3 * size + 0] = clusterBounds[j].lonMax;
		// 	valueCoords[i * 6 * size + 3 * size + 1] = clusterBounds[j].latMax;
		// 	valueCoords[i * 6 * size + 4 * size + 0] = clusterBounds[j].lonMax; //last index of prev triangle
		// 	valueCoords[i * 6 * size + 4 * size + 1] = clusterBounds[j].latMax;
		// 	i++;
		// }
		// */
  //   	attribute.value = valueCoords;
  //       this.state.model.setVertexCount(valueCoords.length/2); //FIXME ! - why can't i access attribute value arrays?
	}
	
	calculateValues(attribute){
		// const {frp_data, modis_data, goes_data, clusters} = this.props;
  // 		const size = attribute.size;
  		
  // 		/*
  //   	const modis_polygons = modis_data.features.filter(function(item) { return item.geometry.type == 'Polygon'; });
  //   	const goes_polygons = goes_data.features.filter(function(item) { return item.geometry.type == 'Polygon'; });
  // 		const modis_points = frp_data.features.filter(function(item) { return item.geometry.type == 'Point'; })
  // 		const goes_points = goes_data.features.filter(function(item) { return item.geometry.type == 'Point'; });
		// */
  // 		var idx = 0;
  // 		var stop = 5;
  // 		/*
  // 		const goes_points_in_ca = goes_points.filter(point => this.isInCalifornia(point.geometry.coordinates[0], point.geometry.coordinates[1]));
  // 		const modis_points_in_ca = modis_points.filter(point => this.isInCalifornia(point.geometry.coordinates[0], point.geometry.coordinates[1]));
  // 		*/
  // 		/*
  // 		var valueValues = new Float32Array(
  // 			(goes_points_in_ca.length + modis_points_in_ca.length)*6*3 - 3 + clusters.indices.length * 6 * 3); //FIXME!
  // 			*/

  // 		var valueValues = new Float32Array(
  // 			frp_data.length*6*3 - 3
  // 		);
  		
  //   	for (var i = 0; i < goes_data.length; i++){
		// 	for (var j = 0; j < stop; j++){
		// 		valueValues[idx + size * j + 0] = 0.5; //fixme: need to get temperature -- FRP relation (and map the 'unknowns' somehow)
		// 		valueValues[idx + size * j + 1] = 1.0; //confidence
		// 		valueValues[idx + size * j + 2] = 0.0; // 0.0 = most recent
		// 	}
		// 	idx += stop * size;
		// 	stop = 6;
  //   	}
    	
  //   	for (var i = 0; i < frp_data.length; i++){
		// 	for (var j = 0; j < stop; j++){
		// 		valueValues[idx + size * j + 0] = getScaledFRP(parseFloat(frp_data[i].power)); //frp 
		// 		valueValues[idx + size * j + 1] = getScaledConfidence(parseFloat(frp_data[i].confidence)); //confidence
		// 		valueValues[idx + size * j + 2] = 0.0;//getScaledTime(modis_polygons[i].properties.styleUrl); // 0.0 = most recent
		// 		//FIXME! ^^
		// 	}
		// 	idx += stop * size;
		// 	stop = 6;
  //   	}
  //   	/*
		// const clusterIndices = clusters.indices;
		// for (var i = 0; i < clusterIndices.length; i++){
		// 	for (var j = 0; j < stop; j++){
		// 		valueValues[idx + size * j + 0] = 0.0;
		// 		valueValues[idx + size * j + 1] = 1.0;
		// 		valueValues[idx + size * j + 2] = 0.0;
		// 	}
		// 	idx += stop * size;
		// }
		// */
		// attribute.value = valueValues;
	}
	
    getInterpPositions(attribute){
        const { points } = this.props;
        const typedPoints = new Float32Array(points);
        attribute.value = typedPoints;
        this.state.model.setVertexCount(points.length/2); //FIXME ! - why can't i access attribute value arrays?
    }
    
    getInterpValues(attribute){
        const {values} = this.props;
        const typedValues = new Float32Array(values);
        attribute.value = typedValues;
    }
	
/* 
	helper functions
*/  
    createTexture(gl, opt){
  		const textureOptions = Object.assign(
		{
			format: gl.RGBA,
			dataFormat: gl.RGBA,
			type: gl.UNSIGNED_BYTE,
			parameters: {
			  [gl.TEXTURE_MAG_FILTER]: gl.NEAREST,
			  [gl.TEXTURE_MIN_FILTER]: gl.NEAREST,
			  [gl.TEXTURE_WRAP_S]: gl.CLAMP_TO_EDGE,
			  [gl.TEXTURE_WRAP_T]: gl.CLAMP_TO_EDGE
		},
		pixelStore: {[gl.UNPACK_FLIP_Y_WEBGL]: true}
		},
		opt
		);

		return new Texture2D(gl, textureOptions);    
	}
	
	getScaledFRP(description){
		var strings = description.split('FRP: </b>');
		const FRP = parseFloat(strings[1].split('\n')[0]);
		if (FRP < 400) { return (FRP/400.); }
		else { return 1.0; }		
	}
	
	getScaledConfidence(description){
		var strings = description.split('Confidence: </b>');
		const c = parseInt(strings[1].split('\n')[0]);
		return (c/100.);
	}
	
	getScaledTime(styleUrl){
		const styleUrls = ['# 00_to_06hr_fire ', '# 06_to_12hr_fire ', '# 12_to_24hr_fire ', '# prev_6_days_fire '];
  	  	// 0: 1
  	  	// 1: 0.9
  	  	// 2: 0.8
  	  	// 3: 0.7
		return 1 - (styleUrls.indexOf(styleUrl))*.1;
	}
	
	getColorScale(colors, greys) {
    	const canvas = document.createElement('canvas');
		canvas.width = canvas.clientWidth;
		canvas.height = canvas.clientHeight;
		const ctx = canvas.getContext('2d');
	
		canvas.width = 16;
		canvas.height = 16;
	
		const colorGradient = ctx.createLinearGradient(0, 0, 16, 0);
		for (const stop in colors) {
			colorGradient.addColorStop(+stop, colors[stop]);
		}
		ctx.fillStyle = colorGradient;
		ctx.fillRect(0, 0, 16, 1);
		var colorData = new Uint8Array(ctx.getImageData(0, 0, 16, 1).data);
	
		const greyGradient = ctx.createLinearGradient(0, 0, 16, 0);
		for (const stop in greys) {
			greyGradient.addColorStop(+stop, greys[stop]);
		}
		ctx.fillStyle = greyGradient;
		ctx.fillRect(0, 0, 16, 1);
		var greyData = new Uint8Array(ctx.getImageData(0, 0, 16, 1).data);
	
		for (var i = 0; i < 16; i++){
			var gradient = ctx.createLinearGradient(0, 0, 16, 1);
			var idx = i*4;
			gradient.addColorStop(0.0,'rgba(' + colorData[idx] + ',' + colorData[idx+1] + ',' + colorData[idx+2] + ',' + colorData[idx+3] + ')');
			gradient.addColorStop(1.0,'rgba(' + greyData[idx] + ',' + greyData[idx+1] + ',' + greyData[idx+2] + ',' + greyData[idx+3] + ')');
			ctx.fillStyle = gradient;
			ctx.fillRect(0, i, 16, i+1);
		}
		return new Float32Array(ctx.getImageData(0, 0, 16, 16).data);
	}
}

SatelliteDetectionLayer.layerName = 'SatelliteDetectionLayer';
SatelliteDetectionLayer.defaultProps = defaultProps;