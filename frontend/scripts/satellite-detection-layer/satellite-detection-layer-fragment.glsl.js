export default `\
#define SHADER_NAME satellite-detection-layer-fragment-shader

#ifdef GL_ES
precision highp float;
#endif

varying vec2 texCoord;
varying float opacity;
varying float interp_on;
uniform sampler2D colorTexture;
//uniform sampler2D interpTexture;

void main(void) { 
	if (interp_on != 1.0){
		gl_FragColor = vec4(texture2D(colorTexture, vec2(texCoord.s, texCoord.t)).rgb/255., opacity);
	}
	else {
		gl_FragColor = vec4(texture2D(colorTexture, vec2(texCoord.s, texCoord.t)).rgb/255., opacity);
	}
}
`;