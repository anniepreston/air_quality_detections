     /* global window,document */
import React, {Component, Fragment} from 'react';
import ReactDOM, {render} from 'react-dom';
import MapGL, {StaticMap} from 'react-map-gl';
import {Container, Row, Col, setConfiguration} from 'react-grid-system';
import DeckGLOverlay from './deckgl-overlay.js';
import GoldenLayout from 'golden-layout';
import {connect} from 'react-redux';
import {updateBBox, updateSensorDisplay, updateData} from './dataActions';
import geoViewport from '@mapbox/geo-viewport';

import {xml as requestXml} from 'd3-request';

//should hide this ...
const MAPBOX_TOKEN = 'pk.eyJ1IjoiYWtwcmVzdG8iLCJhIjoiY2pjY2I2MWpwMGV3ejJ3bnR2M3N3djB1byJ9._0l5wFhIngy5RywtZMPVlw'; // eslint-disable-line

export class Primary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        ...DeckGLOverlay.defaultViewport,
        width: 0.8*window.innerWidth,
        height: window.innerHeight
      },
      points: null,
      values: null,
      sensorflag: true, //FIXME: removing sensorflag will keep updating sensors when moving the view, but there will be a load of update messages.
      mapping: 'color-frp',
      mapStyle: 'mapbox://styles/akpresto/cjxxmvzpm01891dmqzg7a97fs',
      bbox: {
        nwlat: 0,
        nwlng: 0,
        selat: 0,
        selng: 0,
        zoom: 0
      }
	  }
    setConfiguration({ gutterWidth: -30 });
  }
  componentDidMount() {
    //this.props.dispatch(fetchInterpolation());
    window.addEventListener('resize', this._resize.bind(this));
    this._resize();
    this.props.dispatch(updateSensorDisplay(true));
  }

  _resize() {
    this._onViewportChange({
      width: 0.78*window.innerWidth,
      height: 0.9*window.innerHeight
    });
  }

  _onViewportChange(viewport) {
    const mapGL = this.mapRef.getMap();
    const bounds = mapGL.getBounds();
    const zoom = mapGL.getZoom();
    const {sensorDisplay, renderMode} = this.props;
    const {sensorflag} = this.state;


    this.setState({
      viewport: {...this.state.viewport, ...viewport},
      //approximating the corners of the bounding box using the other corners -- can we assume this is right for smallish areas? i don't love it
      bbox: {
        nwlat: bounds._ne.lat,
        nwlng: bounds._sw.lng,
        selat: bounds._sw.lat,
        selng: bounds._ne.lng,
        zoom: zoom
      }
    });

    this.props.dispatch(updateBBox(this.state.bbox, this.state.viewport, false));

    if(sensorDisplay){
      var invalid_date = "";
			var invalid_predictionMethod = "";
      if(sensorflag){
        this.props.dispatch(updateData(renderMode['detections'], this.state.bbox, invalid_predictionMethod, invalid_date));
        this.setState({sensorflag: false});
      }
    }
  }

  render() {
    //FIXME: not the best place for this
    const {error, loading, sensorData, interpData, renderMode, viewMode, viewport_lat, viewport_lng, updateCountry, bbox} = this.props;
    const {viewport, texture_data, mapping, mapStyle} = this.state;
    var smallHeight = window.innerHeight*0.28; //adjusted height for small multiples
    var ratio = 0.78*window.innerWidth/(0.9*window.innerHeight);
    var smallWidth = ratio * smallHeight;
    var width = 0.78*window.innerWidth;

    if (updateCountry == true){
      viewport['latitude'] = viewport_lat;
      viewport['longitude'] = viewport_lng;
      viewport['zoom'] = bbox['zoom']; //FIXME: read the default zoom from the countries.json file
    }

    //Small Multiples
    if (viewMode == 'small multiples'){
      if (interpData != null && interpData['results'].length > 0){
        var nwlng = interpData['x'].reduce(function(a, b){
              return Math.min(a, b);
            });
        var selat = interpData['y'].reduce(function(a, b){
              return Math.min(a, b);
            }); //selat
        var selng = interpData['x'].reduce(function(a, b){
              return Math.max(a, b);
            }); //selng
        var nwlat = interpData['y'].reduce(function(a, b){
              return Math.max(a, b);
            }); //nwlat
        var smallViewport = geoViewport.viewport([
            bbox['nwlng'], //nwlng,
            bbox['selat'], //selat,
            bbox['selng'], //selng,
            bbox['nwlat'] //nwlat
        ], [0.5*smallWidth, 0.5*smallHeight], 0, 20, 256, true);
      }
      else { //FIXME! this doesn't seem to be working
        var smallViewport = geoViewport.viewport([
          bbox['nwlng'],
          bbox['selat'],
          bbox['selng'],
          bbox['nwlat']
        ], [0.5*smallWidth, 0.5*smallHeight], 0, 20, 256, true);
      }

      var lon = smallViewport['center'][0],
          lat = smallViewport['center'][1],
          zoom = smallViewport['zoom'];
      var multiplesViewport = {
        'longitude': lon,
        'latitude': lat,
        'zoom': zoom
      };
      // need to figure out the viewport for these maps, given the bounding box of the full map
      return (
          <div width={width}>
            <Row justify="center" align="center" width={width}>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={0} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
                <div class="grid-padding"/>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={1} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
                <div class="grid-padding"/>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={2} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
            </Row>
            <br />
            <Row justify="center" align="center" gutterWidth={12}>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={3} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
                <div class="grid-padding"/>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={4} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
                <div class="grid-padding"/>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={5} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
            </Row>
            <br />
            <Row justify="center" align="center" gutterWidth={12}>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={6} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
                <div class="grid-padding"/>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={7} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
                <div class="grid-padding"/>
                <StaticMap
                  longitude={lon}
                  latitude={lat}
                  zoom={zoom}
                  width={smallWidth}
                  height={smallHeight}
                  mapStyle={mapStyle}
                  mapboxApiAccessToken={MAPBOX_TOKEN}
                  position='absolute'
                >
                  <DeckGLOverlay idx={8} viewport={multiplesViewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
                </StaticMap>
            </Row>
          </div>
      );
    }
    //Other viewModes only need one mapview
    else {

      return(
        <MapGL
          {...viewport}
          ref={(map) => { this.mapRef = map; }}
          id='left_map'
          mapStyle={mapStyle}
          onViewportChange={this._onViewportChange.bind(this)}
          mapboxApiAccessToken={MAPBOX_TOKEN}
        >
        <DeckGLOverlay viewport={viewport} mapping={mapping} sensorData={sensorData} interpData={interpData} renderMode={renderMode} viewMode={viewMode} bbox={bbox}/>
        </MapGL>
      );
    }
  }
}

const mapStateToProps = state => ({
  sensorData: state.items.sensorData,
  interpData: state.items.interpData,
  renderMode: state.renderMode,
  viewMode: state.viewMode,
  updateCountry: state.updateCountry,
  viewport_lat: state.viewport_lat,
  viewport_lng: state.viewport_lng,
  bbox: state.bbox,
  loading: state.loading,
  error: state.error,
  sensorDisplay: state.sensorDisplay
});

export const PrimaryContainer = connect(mapStateToProps)(Primary);
