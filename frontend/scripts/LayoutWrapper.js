	import GoldenLayout from 'golden-layout';
import { Provider, connect } from 'react-redux';
import { PrimaryContainer} from './views.js';
import { TimelineContainer } from './timelineView.js';
import { ControlsContainer } from './controls.js';
import { ViewModeContainer } from './viewMode.js';
import { Legend } from './legend.js';
import PropTypes from 'prop-types';
import syncMove from '@mapbox/mapbox-gl-sync-move';

class LayoutWrapper extends React.Component {
	componentDidMount() {

		const config = {
			height: 90,
			content: [{
				type: 'column',
				height: 90,
				content: [{
					type: 'row',
					height: 150,
					content: [{
						type: 'column',
						height: 150,
						width: 20,
						content: [{
							type: 'react-component',
							component: 'ControlsContainer',
							componentName: 'Settings',
							title: 'Settings',
							height: 60,
						},{
							type: 'react-component',
							component: 'ViewModeContainer',
							componentName: 'View Mode',
							title: 'View Mode',
							height: 90,
						}]
					},{
						type: 'column',
						height: 100,
						width: 80,
						content: [{
							type: 'react-component',
							component: 'PrimaryContainer',
							componentName: 'Air Quality Map',
							title: 'Air Quality Map',
							height: 90,
							width: 80,
						},{
							type: 'react-component',
							component: 'Legend',
							componentName: 'Legend',
							title: 'Legend',
							height: 10,
							width: 80,
						}]
					}]
				}]
			}]
		};

		function wrapComponent(Component, store) {
			class Wrapped extends React.Component {
				render() {
					console.log("wrapping props: ", this.props);
					return React.createElement(
						Provider,
						{ store: store },
						React.createElement(Component, this.props)
					);
				}
			}
			return Wrapped;
		};

		var layout = new GoldenLayout(config, this.layout);
		layout.registerComponent('PrimaryContainer', wrapComponent(PrimaryContainer, this.context.store));
		layout.registerComponent('TimelineContainer', wrapComponent(TimelineContainer, this.context.store));
		layout.registerComponent('ControlsContainer', wrapComponent(ControlsContainer, this.context.store));
		layout.registerComponent('ViewModeContainer', wrapComponent(ViewModeContainer, this.context.store));
		layout.registerComponent('Legend', wrapComponent(Legend, this.context.store));

		layout.init();

		window.addEventListener('resize', () => {
			layout.updateSize();
		});
	}

	render() {
		return React.createElement('div', { className: 'goldenLayout', ref: input => this.layout = input });
	}
}

// ContextTypes must be defined in order to pass the redux store to exist in
// "this.context". The redux store is given to GoldenLayoutWrapper from its
// surrounding <Provider> in index.jsx.

LayoutWrapper.contextTypes = {
	store: PropTypes.object.isRequired
};

export default LayoutWrapper;
