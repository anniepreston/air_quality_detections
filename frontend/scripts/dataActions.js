export const FETCH_BEGIN = 'FETCH_BEGIN';
export const FETCH_FAILURE = 'FETCH_FAILURE';

export const FETCH_INTERP_SUCCESS = 'FETCH_INTERP_SUCCESS';
export const UPDATE_BBOX_SUCCESS = 'UPDATE_BBOX_SUCCESS';
export const UPDATE_RENDER_MODE_SUCCESS = 'UPDATE_RENDER_MODE_SUCCESS';
//export const UPDATE_PREDICTION_METHOD_SUCCESS = 'UPDATE_PREDICTION_METHOD_SUCCESS';
export const UPDATE_VIEW_MODE_SUCCESS = 'UPDATE_VIEW_MODE_SUCCESS';
export const UPDATE_DATA_SUCCESS = 'UPDATE_DATA_SUCCESS';
export const UPDATE_SENSOR_DISPLAY_SUCCESS = 'UPDATE_SENSOR_DISPLAY_SUCCESS';
export const ANIMATE_DATA_SUCCESS = 'ANIMATE_DATA_SUCCESS';

import { postAir, postDetectionInterpolation, postAnimation } from './api.js';

export const fetchBegin = () => ({
	type: FETCH_BEGIN
});

export const fetchFailure = error => ({
  type: FETCH_FAILURE,
  payload: { error }
});

export const fetchInterpSuccess = data => ({
  type: FETCH_INTERP_SUCCESS,
  payload: { values: data.values, points: data.points }
});

export const updateBBoxSuccess = data => ({
  type: UPDATE_BBOX_SUCCESS,
  payload: { bbox: data.bbox, viewport: data.viewport, updateCountry: data.updateCountry}
});

export const updateRenderModeSuccess = data => ({
  type: UPDATE_RENDER_MODE_SUCCESS,
  payload: { renderMode: data.renderMode }
});

export const updateViewModeSuccess = data => ({
  type: UPDATE_VIEW_MODE_SUCCESS,
  payload: { viewMode: data.viewMode }
});
/*
export const updatePredictionMethodSuccess = data => ({
  type: UPDATE_PREDICTION_METHOD_SUCCESS,
  payload: { predictionMethod: data.predictionMethod, sensorData: data.sensorData, interpData: data.interpData }
});*/

export const updateDataSuccess = data => ({
  type: UPDATE_DATA_SUCCESS,
  payload: { sensorData: data.sensorData, interpData: data.interpData }
});

export const updateSensorDisplaySuccess = data => ({
	type: UPDATE_SENSOR_DISPLAY_SUCCESS,
	payload: {sensorDisplay: data.sensorDisplay}
});

export const animateDataSuccess = data => ({
  type: ANIMATE_DATA_SUCCESS,
  payload: { sensorData: data.sensorData, interpData: data.interpData }
});


export function updateBBox(bbox, viewport, updateCountry){
  return function (dispatch) {
    return dispatch(updateBBoxSuccess({bbox: bbox, viewport: viewport, updateCountry: updateCountry}));
  }
}

export function updateRenderMode(renderMode){
    return function (dispatch) {
      return dispatch(updateRenderModeSuccess({renderMode: renderMode}));
    }
}

export function updateViewMode(viewMode){
  return function (dispatch) {
    return dispatch(updateViewModeSuccess({ viewMode: viewMode }));
  }
}

export function updateSensorDisplay(sensorDisplay){
	return function (dispatch) {
		return dispatch(updateSensorDisplaySuccess({ sensorDisplay: sensorDisplay}));
	}
}

/*
export function updatePredictionMethod(sourceList, bbox, predictionMethod){
  return function (dispatch) {
      dispatch(fetchBegin());
      return postAir({
        'sourceList': sourceList,
        'nwlat': bbox.nwlat.toString(),
        'selat': bbox.selat.toString(),
        'nwlng': bbox.nwlng.toString(),
        'selng': bbox.selng.toString(),
        'zoom': bbox.zoom.toString(),
        'resolution': 'low', //FIXME! request high or low depending on speed threshold for the area (need to test first)
        'predictionMethod': predictionMethod
      }).then(function(data){
        dispatch(updatePredictionMethodSuccess(data));
      }).catch(function(error){
        dispatch(fetchFailure(error));
      })
  }
}*/

export function updateData(sourceList, bbox, predictionMethod, date){
    return function(dispatch) {
      dispatch(fetchBegin());

      return postAir({
        'sourceList': sourceList,
        'nwlat': bbox.nwlat.toString(),
        'selat': bbox.selat.toString(),
        'nwlng': bbox.nwlng.toString(),
        'selng': bbox.selng.toString(),
        'zoom': bbox.zoom.toString(),
        'resolution': 'low', //FIXME! request high or low depending on speed threshold for the area (need to test first)
        'predictionMethod': predictionMethod,
        'date': date
      }).then(function(data){
        dispatch(updateDataSuccess(data));
      }).catch(function(error){
        dispatch(fetchFailure(error));
      })
    }
}

export function animateData(index){
  return function(dispatch) {
    dispatch(fetchBegin());

    return postAnimation({
      'index': index
    }).then(function(data){
      dispatch(animateDataSuccess(data));
    }).catch(function(error){
      dispatch(fetchFailure(error));
    })
  }
}

function formatDate(item){
	if (item < 10){
		var formatted = '0' + String(item);
	}
	else {
		var formatted = String(item);
	}
	return formatted;
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

const getClusters = data =>  {
  const clusters = clusterDetections(data);
  return clusters;
}
