/* global window,document */
import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import GoldenLayout from 'golden-layout';
import {connect} from 'react-redux';
import { fetchCurrentPerimeter, fetchSatelliteRange, fetchTargetPerimeter } from './dataActions';
import * as d3 from 'd3';

export class Timeline extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: window.innerHeight,
      width: 0,
      satelliteRange: {
        start_date: null,
        end_date: null
      }
	  }
    this.drawPerimeters = this.drawPerimeters.bind(this);
    this.parseDate = this.parseDate.bind(this);
    this.updateCurrentPerimeter = this.updateCurrentPerimeter.bind(this);
    this.updateSatelliteRange = this.updateSatelliteRange.bind(this);
    this.updateTargetPerimeter = this.updateTargetPerimeter.bind(this);
  }
  componentDidMount() {
    //this.props.dispatch(fetchInterpolation());
  }

  componentDidUpdate() {
    const {perimeters} = this.props;
    const {height} = this.state;
    if (typeof(perimeters) !== 'undefined' && perimeters.length > 0){
        this.drawPerimeters();
    }
  }

  drawPerimeters() {
    //view things
    const node = this.node;
    d3.select(node).selectAll("*").remove();
    d3.select(node).selectAll("svg").remove();
    var {height, satelliteRange} = this.state;
    var {perimeters, currentPerimeter} = this.props;

    //get parameters / set defaults
    const n = perimeters.length;
    const start_date = this.parseDate(perimeters[0].date); //i.e., perimeter start date
    const end_date = this.parseDate(perimeters[n-1].date);

    if (typeof(currentPerimeter) == 'undefined') currentPerimeter = 0;
    if (satelliteRange.start_date == null) {
        const next_date = new Date(start_date.getTime() + 1000*60*60*24*1*0.5); // Offset by HALF OF A day;
        //this.setState({ satelliteRange: {'start_date': start_date, 'end_date': next_date} });
        satelliteRange = {'start_date': start_date, 'end_date': next_date};
    }

    const width_per_perimeter = 120;
    const perim_y_offset = 61;

    var timeScale = d3.scaleTime()
        .domain([start_date, end_date])
        .range([40, width_per_perimeter*n*3]);

    var perimeterPolygons = perimeters.map(p => p.perimeter);
    var perimeterDates = perimeters.map(p => this.parseDate(p.date));

    var projection = d3.geoMercator()
      .fitExtent([[10,10], [110, 110]], perimeterPolygons[n-1]);

    var geoPath = d3.geoPath()
      .projection(projection);
  
    var timeAxis = d3.axisTop(timeScale)
        .tickArguments([d3.timeDay.every(1)]);

    var timeAxisContainer = d3.select(node).append("svg")
        .attr("width", timeScale(end_date))
        .attr("height", height)
        .attr("x", 0)
        .attr("y", 0)
        .attr("transform", function(){return "translate(" + "0" + "," + "0" + ")"; });
        
    timeAxisContainer.append("g")
        .attr("width", timeScale(end_date))
        .attr("height", height)
        .attr("transform", "translate(0,20)")
        .call(timeAxis);

    timeAxisContainer.selectAll("rect .perim")
        .data(perimeterDates)
        .enter()
        .append("rect")
        .attr("class", "perim")
        .attr("transform", function(d){ return "translate(" + (timeScale(d)) + "," + perim_y_offset + ")"; })
        .attr("fill", "#c6c6c6")
        .attr('stroke', function(d,i){ if (i == currentPerimeter) var stroke = 'black'; else var stroke = 'none'; return stroke; })
        .attr('stroke-width', 2)
        .attr("width", 120)
        .attr("height", 119)
        .attr("opacity", 0.5)
        .on("click", function(d,i){
          handleClick(i);
        })
        .exit().remove();

    var detectionRangeBox = timeAxisContainer.append("g");
    var targetPointer = timeAxisContainer.append("g").attr("width", 40).attr("height", 40);

    function dragged_detect (){
        var dx = d3.event.sourceEvent.offsetX;
        d3.select(node).selectAll(".detect")
            .attr("transform", function() { return "translate(" + (dx + timeScale(start_date)) + "," + 26 + ")" })
    }
    function dragged_target (){
        var dx = d3.event.sourceEvent.offsetX;
        d3.select(node).selectAll(".target")
            .attr("transform", function() { return "translate(" + (dx + timeScale(start_date)) + "," + (perim_y_offset + 146) + ")" })   
    }
    function dragstopped_detect (){
        //get start date:
        const transform = d3.select(node).selectAll(".detect").attr("transform");
        const left_paren = '(';
        const comma = ',';
        var strings = transform.split(left_paren);
        var x_string = strings[1].split(comma);

        //get end date (from width):
        const width = d3.select(node).selectAll(".detect").attr("width");
        const end_x = parseInt(x_string) + parseInt(width);
        sendSatelliteRange({'start_date': timeScale.invert(parseInt(x_string)), 'end_date': timeScale.invert(end_x)});
    }
    function dragstopped_target (){
        const transform = d3.select(node).selectAll(".target").attr("transform");
        const left_paren = '(';
        const comma = ',';
        var strings = transform.split(left_paren);
        var x_string = strings[1].split(comma);
        //const {perimeters, currentPerimeter} = this.props;

        //get end date (from width):
        const width = d3.select(node).selectAll(".detect").attr("width");
        const end_x = parseInt(x_string) + parseInt(width);
        sendTargetPerimeter({'target_date': timeScale.invert(parseInt(x_string)), 'state': perimeters[currentPerimeter].state, 'fire_name': perimeters[currentPerimeter].fire_name, 'perimeter_date': perimeters[currentPerimeter].date});    
    }
    var drag_behavior_detections = d3.drag()
        .on("drag", dragged_detect)
        .on("end", dragstopped_detect);
    var drag_behavior_target = d3.drag()
        .on("drag", dragged_target)
        .on("end", dragstopped_target);

    detectionRangeBox.append("rect")
      .attr("class", "detect")
      .attr("transform", function(){ return "translate(" + (timeScale(satelliteRange.start_date)) + ",26)"; })
      .attr("fill", "#c6c6c6")
      .attr("stroke", "black")
      .attr("stroke-width", 2)
      .attr("height", 24) //?
      .attr("width", function(){ return timeScale(satelliteRange.end_date) - timeScale(satelliteRange.start_date); })
      .attr("opacity", 0.5)
      .call(drag_behavior_detections);

    timeAxisContainer.selectAll("path perimeter")
        .data(perimeterPolygons)
        .enter()
        .append("path")
        .attr("stroke", "black")
        .attr("class", "perimeter")
        .attr("fill", "black")
        .attr("transform", function(d,i){ return "translate(" + (timeScale(perimeterDates[i])) + "," + perim_y_offset + ")"; })
        .attr("d", geoPath);

    timeAxisContainer.append("path")
      .attr("d", d3.symbol().type(d3.symbolTriangle).size([100]))
      .attr("transform", function(){ return "translate(" + (timeScale(satelliteRange.start_date)) + "," + (perim_y_offset + 146) + ")"; })
      .attr("class", "target")
      .style("stroke", "black")
      .attr("opacity", 0.5)
      .style("stroke-width", 4)
      .style("fill", "none")
      .call(drag_behavior_target); //append text here...

    const handleClick = idx => this.updateCurrentPerimeter(idx);
    const sendSatelliteRange = range => this.updateSatelliteRange(range);
    const sendTargetPerimeter = parameters => this.updateTargetPerimeter(parameters);
        /*
        d3.select(this)
            .attr("transform", () => "translate(" + dx + "," + dy + ")");
            */
  }

  updateCurrentPerimeter(idx) {
      this.props.dispatch(fetchCurrentPerimeter(idx));
  }

  updateSatelliteRange(range) {
      var {perimeters, currentPerimeter, mode} = this.props;
      console.log('updating satellite request with mode: ', mode);
      if (typeof(mode) == 'undefined') mode = 'raw detections';
      if (!(currentPerimeter >= 0)) currentPerimeter = 0;
      const start_date = range.start_date;
      const end_date = range.end_date;
      console.log("update satellite range with date range = ", start_date, end_date);
      this.setState({ satelliteRange: { start_date: start_date, end_date: end_date }});
      //send perimeter + date range to server to fetch related satellite detections
      this.props.dispatch(fetchSatelliteRange(start_date, end_date, perimeters[currentPerimeter], mode));
  }

  updateTargetPerimeter(parameters){
      // set state: current target date (so target pointer doesn't get reset)
      this.props.dispatch(fetchTargetPerimeter(parameters));
  }

  parseDate(raw_date) {
    var year = raw_date.slice(0,4);
    var month = raw_date.slice(5,7);
    var date = raw_date.slice(8,10);
    var hour = raw_date.slice(11,13);
    var minute = raw_date.slice(14,16);
    return new Date(year, month, date, hour, minute);
  }
  
  render() {
        return (
          <div style={{'overflowX': 'scroll', 'width': '100%', 'height': '100%'}} ref={node => this.node = node}> 
          </div>
        );
  }
}

const mapStateToProps = state => ({
  perimeters: state.perimeters,
  loading: state.loading,
  error: state.error,
  currentPerimeter: state.currentPerimeter,
  mode: state.mode
});

export const TimelineContainer = connect(mapStateToProps)(Timeline);