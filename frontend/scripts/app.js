import thunkMiddleware from 'redux-thunk';
import LayoutWrapper from './LayoutWrapper';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import { Provider } from 'react-redux';
import reducer from './reducer';
import {render} from 'react-dom';

import '../styles/main.css';

const store = createStore(reducer, applyMiddleware(thunkMiddleware, logger));

render(React.createElement(
	Provider,
	{ store: store },
	React.createElement(LayoutWrapper, null)
), document.body.appendChild(document.getElementById('root')));
