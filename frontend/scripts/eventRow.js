import React from 'react';

export default class EventTable extends React.Component {
    constructor(props) {
        super(props);

        this.selectRow = this.selectRow.bind(this);
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }
    
    shouldComponentUpdate(nextProps) {
        return true;
    }

    render() {
        var columns = [];

        Object.keys(this.props.data).forEach(function(column) {
            columns.push(
                <td key={column}>
                    {this.props.data[column]}
                </td>
            ); 
        }, this);

        return (
            <tr className='fireRow' key={this.props.data['fire_name']} onClick={this.selectRow}>
                {columns}
            </tr>
        );
    }

    selectRow(e) {
        console.log("selected fire: ", this.props.data);
        this.props.setSelectedFire(this.props.data);
    }
}
