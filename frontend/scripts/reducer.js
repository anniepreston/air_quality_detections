import {
  FETCH_BEGIN,
  FETCH_FAILURE,
  UPDATE_BBOX_SUCCESS,
  UPDATE_RENDER_MODE_SUCCESS,
  UPDATE_VIEW_MODE_SUCCESS,
  UPDATE_PREDICTION_METHOD_SUCCESS,
  UPDATE_DATA_SUCCESS,
  ANIMATE_DATA_SUCCESS
} from './dataActions';

const initialState = {
	items: [],
	loading: false,
	error: null,
  renderMode: {'detections': ['AirNow'], 'prediction': ['AirNow']},
  predictionMethod: 'kriging',
  viewMode: 'interpolation',
};

export default function reducer(state = initialState, action){
	switch(action.type) {
		  case FETCH_BEGIN:
      		return {
        		...state,
        		loading: true,
        		error: null
       		};
    	case FETCH_FAILURE:
	        return {
	            ...state,
	            loading: false,
	            error: action.payload.error,
	            items: []
	        };
      case UPDATE_BBOX_SUCCESS:
        return {
          ...state,
          bbox: action.payload.bbox,
        }
    	case UPDATE_RENDER_MODE_SUCCESS:
    		return {
    			...state,
    			renderMode: action.payload.renderMode
    		}
      case UPDATE_VIEW_MODE_SUCCESS:
        return {
          ...state,
          viewMode: action.payload.viewMode
        }
      case UPDATE_PREDICTION_METHOD_SUCCESS:
        return {
          ...state,
          loading: false,
          items: { predictionMethod: action.payload.predictionMethod, sensorData: action.payload.sensorData, interpData: action.payload.interpData }
        }
      case UPDATE_DATA_SUCCESS:
        return {
          ...state,
          loading: false,
          items: { sensorData: action.payload.sensorData, interpData: action.payload.interpData }
        }
      case ANIMATE_DATA_SUCCESS:
        return {
          ...state,
          loading: false,
          items: { sensorData: action.payload.sensorData, interpData: action.payload.interpData }
        }
    	default:
      		return {
        		...state,
        		loading: false,
        		error: null
        	};
	}
}
