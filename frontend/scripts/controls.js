import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';
import * as d3 from 'd3';
import {updateRenderMode, updateData, updateBBox, animateData, updateViewport} from './dataActions';
import {css} from '@emotion/core';
import {BounceLoader} from 'react-spinners';
import DateTimePicker from 'react-datetime-picker';
import moment from 'moment-timezone';

//FIXME: clean up this file!! can consolidate a lot of the styling etc!

//maybe there is a better way to deal with "country"?
var countries = require('./countries.json');
var country = [];
for (var i=0; i< countries.length; i++){
	country.push(countries[i].name);
}

export class Controls extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
      		height: 0.4*window.innerHeight, // adjust the panel size
      		width: 0.2*window.innerWidth,
					sourceIndices: [0],
          methodIndex: 0,
					countryIndex: 0,
					sources: ['AirNow', 'PurpleAir'],
      		viewModes: ['detections', 'interpolation'],
          predictionMethods: ['kriging', 'IDW'],
          date: new Date(),
          iter: 0 //for animation
      	};
    	this.createControls = this.createControls.bind(this);
			this.updateTimeZone = this.updateTimeZone.bind(this);
			this.updateSources = this.updateSources.bind(this);
      this.updatePredictionMethod = this.updatePredictionMethod.bind(this);
			this.updateViewport = this.updateViewport.bind(this);
      this.toggleIndex = this.toggleIndex.bind(this);
      this.loadData = this.loadData.bind(this);
      this.animateData = this.animateData.bind(this);
      this.loadHistoricalData = this.loadHistoricalData.bind(this);
			this.updateCountry = this.updateCountry.bind(this);
      this.onChange = this.onChange.bind(this);
  	}
  	componentDidMount() {
    	this.createControls();
      //this.interval = setInterval(() => this.tick(), 900);
  	}

    componentWillUnmount() {
      //clearInterval(this.interval);
    }

  	componentDidUpdate() {
  		this.createControls();
  	}


		updateSources(i){
			const {sourceIndices, sources} = this.state;
			const {renderMode, bbox} = this.props;
			const newIndices = this.toggleIndex(i, sourceIndices);
			this.setState({sourceIndices: newIndices});

			var sourceList = sources.filter(function(d, i){
				return newIndices.includes(i);
			});

			var newRenderMode = {
				'prediction': sourceList,
				'detections': sourceList
			};
			this.props.dispatch(updateRenderMode(newRenderMode));
		}

    updatePredictionMethod(i){
      // need to re-load the data...
      // FIXME -- possible to store both reults at once?
      const {methodIndex, predictionMethods, sources} = this.state;
      const {bbox} = this.props;
      this.setState({methodIndex: i});

      //FIXME: can get rid of update-prediction-method on backend
      //var method = predictionMethods[i];
      //this.props.dispatch(updatePredictionMethod(sources, bbox, method));
    }

		updateTimeZone(){
			const {countryIndex} = this.state;
			var zone = countries[countryIndex].timezone;
			//print detected user's time zone
			//console.log(moment.tz.guess(true));
			var time = moment.tz(new Date(), zone).format("YYYY/MM/DD HH:mm:ss");
			//only time.format() reflects the time change.
			console.log(zone, time, new Date(time));
			this.onChange(new Date(time));
		}

		updateCountry(i){
			const {countryIndex} = this.state;
			this.setState({countryIndex: i, sources: countries[i].sensors});
			console.log("Switch to: ", countries[i].name);
			this.updateTimeZone();
			this.updateViewport();
			this.initialize();
		}

		updateViewport(){
      console.log("updating viewport...");
			const {countryIndex} = this.state;
			const {viewport} = this.props; //refer to RenderMode?
			var view = countries[countryIndex].viewport;
			var bbox = countries[countryIndex].bbox;
			this.props.dispatch(updateBBox(bbox, view, true));
      this.props.dispatch(updateViewport(view));
		}
		
		initialize(){
			const {sources} = this.state;
			const {bbox} = this.props;
			this.setState({sourceIndices: [0],methodIndex: 0 });
			var newRenderMode = {
				'prediction': sources[0],
				'detections': sources[0]
			};
			this.props.dispatch(updateRenderMode(newRenderMode));
			var invalid_date = "";
			var invalid_predictionMethod = "";
			this.props.dispatch(updateData(newRenderMode['detections'], bbox, invalid_predictionMethod, invalid_date));
		}

    loadData(){ // real-timer
			const {sources, sourceIndices} = this.state;
			this.updateTimeZone();
			console.log("sensor source indices: ", sourceIndices);
      const invalid_date = "";
			const requestSources = sources.filter(function(d, i){
				return sourceIndices.includes(i);
			});

      const {bbox, predictionMethod} = this.props;
			console.log(requestSources);
      this.props.dispatch(updateData(requestSources, bbox, predictionMethod, invalid_date)); //FIXME: rename this function
		}

    animateData(){
      const {iter} = this.state;
      this.props.dispatch(animateData(iter));
    }

    tick() {
      const missing = [7, 15, 16, 17];
      const max = 40;
      const {iter} = this.state;
      var updated = iter + 1;
      if (updated >= max){
        updated = updated - max;
      }
      while (missing.includes(updated)){
        updated += 1;
      }

      this.setState({iter: updated});
      this.props.dispatch(animateData(updated));
    }

    loadHistoricalData(){
      const {sourceIndices, sources, date, methodIndex, predictionMethods} = this.state;
      const {bbox, predictionMethod} = this.props;
      const requestSources = sources.filter(function(d, i){
        return sourceIndices.includes(i);
      })
			var formatteddate  = moment(date).format("YYYY-MM-DD HH:mm:ss");
      this.props.dispatch(updateData(requestSources, bbox, predictionMethods[methodIndex], formatteddate));
    }

    toggleIndex(idx, arr){
      if (arr.includes(idx)) {
        var i = arr.indexOf(idx);
        arr.splice(i, 1);
      }
      else {
        arr.push(idx);
      }
      return arr;
    }

		onChange(date) {
			this.setState({ date: date});
		};

    //draw all the boxes and labels and buttons
  	createControls() {
  		const node = this.node;
  		d3.select(node).selectAll("*").remove();
			const {width, height, sources, sourceIndices, predictionMethods, methodIndex, countryIndex} = this.state;

      const yLoad = 24,
						yCountry = 44,
            ySensors = 54+24, //54
            yMethod = 116+24, //140
            xMargin = 4,
            x2Margin = 120,
            yLoadHistorical = 210;

			const handleSource = idx => this.updateSources(idx);
      const loadData = () => this.loadData();
      const loadHistoricalData = () => this.loadHistoricalData();
      const handleMethod = idx => this.updatePredictionMethod(idx);
      const animateData = () => this.animateData();
			const handleCountry = idx => this.updateCountry(idx);

      //FIXME: consolidate this all in css !

  		var controlsContainer = d3.select(node).append("svg")
  			.attr("width", width)
  			.attr("height", height)
  			.attr("x", 0)
  			.attr("y", 0);

      controlsContainer.append("rect")
        .attr("class", "loadBox")
        .attr("width", 90)
        .attr("height", 20)
        .attr("x", xMargin)
        .attr("y", yLoad - 14)
        .attr("fill", "#d7e3f7")
        .attr("stroke", "#7a7a7a")
        .on("click", function(){
          loadData();
        });

      //adding in animation support...
      /*
      controlsContainer.append("rect")
        .attr("class", "loadBox")
        .attr("width", 90)
        .attr("height", 20)
        .attr("x", xMargin + 120)
        .attr("y", yLoad - 14)
        .attr("fill", "#d7e3f7")
        .attr("stroke", "#7a7a7a")
        .on("click", function(){
          animateData();
        });
        */

				/* //Don't know why this won't work... attrs are old
			controlsContainer.append("select")
				.attr("id", "countryList")
				.attr("hidden", null)
				.attr("x", xMargin)
				.attr("y", yCountry)
				.on("change", function(){
					handleCountry();
				})
				.selectAll("option")
				.data(country)
				.enter()
				.append("option")
				.text(function(d){return d;})
				.attr("value", function(d){return d;});
				*/

      controlsContainer.append("text")
        .text("load current")
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px")
        .attr("fill", "#7a7a7a")
        .attr("text-anchor", "middle")
        .attr("x", xMargin + 45)
        .attr("y", yLoad);

        /*
      controlsContainer.append("text")
        .text("animate")
        .attr("font-family", "sans-serif")
        .attr("font-size", "12px")
        .attr("fill", "#7a7a7a")
        .attr("text-anchor", "middle")
        .attr("x", xMargin + 45 + 120)
        .attr("y", yLoad);
        */

      controlsContainer.append("text")
        .text("sources")
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px")
        .attr("fill", "#7a7a7a")
        .attr("x", xMargin)
        .attr("y", ySensors);

      controlsContainer.append("text")
        .text("interpolation method")
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px")
        .attr("fill", "#7a7a7a")
        .attr("x", xMargin)
        .attr("y", yMethod);

      controlsContainer.selectAll("rect .sensorBox")
        .data(sources)
        .enter()
        .append("rect")
        .attr("class", "sensorBox")
        .attr("width", 12)
        .attr("height", 12)
        .attr("x", xMargin)
        .attr("y", function(d, i){ return ySensors + 6 + i*18;})//{ return 82+i*16; })
        .attr("stroke", function(d,i){ if (sourceIndices.includes(i)) return "#7a7a7a"; else return "#c6c6c6";})
        .attr("fill", "#d7e3f7")
        .attr("fill-opacity", function(d,i){ if (sourceIndices.includes(i)) return 1; else return 0;})
        .on("click", function(d,i){
						console.log("clicked!");
						handleSource(i);
        });

      controlsContainer.selectAll('.sensorText')
        .data(sources)
        .enter()
        .append("text")
        .attr("x", xMargin-8)
        .attr("y", 8)
        .attr("class", "sensorText")
        .attr("transform", function(d,i){ return "translate(28," + (ySensors + 8 +i*18) + ")"; })
        .attr("fill", function(d,i){ if (sourceIndices.includes(i)) return "#7a7a7a"; else return "#c6c6c6";})
        .text(function(d){ return d; })
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px");

      controlsContainer.selectAll("rect .methodBox")
        .data(predictionMethods)
        .enter()
        .append("rect")
        .attr("class", "methodBox")
        .attr("width", 12)
        .attr("height", 12)
        .attr("x", xMargin)
        .attr("y", function(d,i){ return yMethod + 6 +i*18; })
        .attr("stroke", function(d,i){ if (methodIndex == i) return "#7a7a7a"; else return "#c6c6c6";})
        .attr("fill", "#d7e3f7")
        .attr("fill-opacity", function(d,i){ if (methodIndex == i) return 1; else return 0;})
          .on("click", function(d,i){
            console.log("clicked!");
            handleMethod(i);
        });

      controlsContainer.selectAll(".methodText")
        .data(predictionMethods)
        .enter()
        .append("text")
        .attr("x", xMargin-8)
        .attr("y", 8)
        .attr("class", "methodText")
        .attr("transform", function(d,i){ return "translate(28," + (yMethod + 8 +i*18) + ")"; })
        .attr("fill", function(d,i){ if (methodIndex == i) return "#7a7a7a"; else return "#c6c6c6";})
        .text(function(d){ return d; })
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px");

      controlsContainer.append("rect")
        .attr("class", "loadBox")
        .attr("width", 90)
        .attr("height", 20)
        .attr("x", xMargin)
        .attr("y", yLoadHistorical - 14)
        .attr("fill", "#d7e3f7")
        .attr("stroke", "#7a7a7a")
        .on("click", function(){
          loadHistoricalData();
        });

      controlsContainer.append("text")
        .text("load historical")
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px")
        .attr("fill", "#7a7a7a")
        .attr("text-anchor", "middle")
        .attr("x", xMargin + 45)
        .attr("y", yLoadHistorical);

			d3.select("#countryList")
				.selectAll("option")
				.data(country)
				.enter()
				.append("option")
				.text(function(d){return d;})
				.attr("value", function(d,i){return i;});

			//Somehow this code only works when they are separated.
			d3.select("#countryList")
				.on("change", function(i){
					var newCountryIdx = d3.select(this).property("value");
					handleCountry(newCountryIdx);
				});

      var colorScale = d3.scaleQuantize()
        .domain([0,350])
        //.range(["#abdda4", "#e6f598", "#fee08b", "#f46d43", "#d53e4f", "#d53e4f", "#9e0142"]);
        .range(["#d0ebef", "#f9f8c2", "#fed385", "#f88e53", "#dd4030", "#dd4030", "#a50026"]);

  	}

  	render() {
        return (
          <div>
          <div style={{'width': '100%', 'height': '100%', 'position': 'absolute'}} ref={node => this.node = node} />
					<div className='spin-loader' style={{'left': '128px', 'top': '5px', 'position': 'absolute'}}>
						<BounceLoader
							sizeUnit={"px"}
							size={50}
							color={'#d7e3f7'}
							loading={this.props.loading}
						/>
					</div>
					<div style={{'left': '4px', 'top': '40px', 'position': 'absolute'}}>
						<select id="countryList"> </select>
					</div>
          <div className='calendar' style={{'left': '4px', 'top': '240px', 'position': 'absolute'}}>
            <DateTimePicker
              onChange={this.onChange}
              value={this.state.date}
            />
          </div>
          </div>
        );
  	}
}

const mapStateToProps = state => ({
  bbox: state.bbox,
	viewport: state.viewport,
  predictionMethod: state.predictionMethod,
  renderMode: state.renderMode,
	countryIndex: state.countryIndex,
  loading: state.loading,
  error: state.error,
});

export const ControlsContainer = connect(mapStateToProps)(Controls);
