import React, {Component} from 'react';
import DeckGL from '@deck.gl/react';
import { GeoJsonLayer, GridCellLayer, TextLayer } from '@deck.gl/layers';
import SatelliteDetectionLayer from './satellite-detection-layer';
import UncertaintyGridLayer from './uncertainty-grid-layer';
import circleToPolygon from 'circle-to-polygon';
import * as d3 from 'd3';
import * as vsup from 'vsup';
import * as turf from '@turf/turf';

export default class DeckGLOverlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sensorData: null,
      mapping: 'binned',
      //parameters for rendering 'circles':
      numberOfEdges: 32,                      //optional; defaults to 32
      //range for AQI values
      min: 0,
      max: 500,
      iter: 0, // for animation
      nCells: 3., // subdivided square -- for DOT VIEW mode -- will be nCells x nCells
      k_default: 6, //for IDW
      p_default: 3,
      nMultiples: 9, //for small multiples
    };

  }

  static get defaultViewport() {
    return {
      longitude: -115, //-115, -122.4
      latitude: 37, //37, 37.8
      zoom: 5.5, //5.5, 8.2
      maxZoom: 20,
      pitch: 0,
      bearing: 0
    };
  };

  binColor(bin){
    //return the color representing this range of AQI
    const bins = [0, 1, 2, 3, 4, 5];
    //const colors = ["#abdda4", "#e6f598", "#fee08b", "#f46d43", "#d53e4f", "#9e0142"];
    const colors = ["#d0ebef", "#f9f8c2", "#fed385", "#f88e53", "#dd4030", "#a50026"];
    var idx = bins.indexOf(bin)
    if (idx != -1){
      return colors[idx];
    }
    else return "#000000";
  }

  scaleCircleRadius(zoom, source) {
    //for viewing individual sensor readings
    //use anecdotal scaling data to create a point-slope equation
    let y1 = 1500., //radius
      y2 = 2000., //testing! zoomed out radius
      x1 = 8.2, //zoom level
      x2 = 7.3; //default zoom
    //piecewise equation: above zoom ~= 9.5, radius stays constant
    const slope = (y2-y1)/(x2-x1);

    //return radius (in meters)
    var radius = (slope*(zoom-x1) + y1);
    if (zoom > 9.5){
      radius = (slope*(9.5-x1) + y1);
    }
    if (source == 'PurpleAir' || source == 'Airbox') {
      radius = 0.7 * radius;
    }
    else {
      radius = 1.2 * radius;
    }
    return radius;
  }

  scaleCircleOpacity(source) {
    if (source == 'PurpleAir' || source == 'Airbox'){
      return 204;
    }
    else {
      return 255;
    }
  }

  scaleLineWidth(source) {
    if (source == 'PurpleAir' || source == 'Airbox'){
      return 10;
    }
    else {
      return 20;
    }
  }

  parseSensorData(sensorData) {
    var jsonData = {
      'type': 'FeatureCollection',
      'features': []
    }
    var outlineData = {
      'type': 'FeatureCollection',
      'features': []
    }
    const {bbox, renderMode} = this.props;
    const zoom = bbox['zoom'];
    const {radius, numberOfEdges, min, max, mapping} = this.state;

    var colorScale;
    const sources = renderMode['detections'];

    if (mapping == 'binned'){
      //map AQI to binned color corresponding to health risk
      colorScale = d3.scaleQuantize()
        .domain([0,350])
        //.range(["#abdda4", "#e6f598", "#fee08b", "#f46d43", "#d53e4f", "#d53e4f", "#9e0142"]);
        .range(["#d0ebef", "#f9f8c2", "#fed385", "#f88e53", "#dd4030", "#dd4030", "#a50026"]);
    }
    else {
      colorScale = d3.scaleSequential(d3.interpolateInferno)
        .domain([max,min]);
    }

    for (var i = 0; i < sensorData['ID'].length; i++){
      var valueColor = d3.rgb(colorScale(sensorData['aqi'][i]));
      var opacity = this.scaleCircleOpacity(sensorData['source'][i]);
      var color = [valueColor.r, valueColor.g, valueColor.b, opacity];
      var lineColor = [173, 173, 173, 255];
      var coordinates = [sensorData['lon'][i], sensorData['lat'][i]];
      var pm25 = [sensorData['pm25'][i]];
      var aqi = [sensorData['aqi'][i]];
      var lineWidth = this.scaleLineWidth(sensorData['source'][i]);
      let polygon = circleToPolygon(coordinates, this.scaleCircleRadius(zoom, sensorData['source'][i]), numberOfEdges);
      let outlinePolygon = circleToPolygon(coordinates, 1.2*this.scaleCircleRadius(zoom, sensorData['source'][i]), numberOfEdges);

      polygon['properties'] = {
          'fillColor': color,
          'lineWidth': lineWidth,
          'lineColor': lineColor
        }

      outlinePolygon['properties'] = {
        'fillColor': [173, 173, 173, 173],
        'lineWidth': lineWidth,
        'lineColor': lineColor
      }

      // prepend if it's an Airnow source, to get rid of occlusion
      if (sensorData['source'][i] == 'PurpleAir' || sensorData['source'][i] == 'Airbox') {
        jsonData.features.push({
          'type': 'Feature',
          'geometry': polygon
        });
        outlineData.features.push({
          'type': 'Feature',
          'geometry': outlinePolygon
        })
      }
      else {
        jsonData.features.unshift({
          'type': 'Feature',
          'geometry': polygon
        });
        outlineData.features.unshift({
          'type': 'Feature',
          'geometry': outlinePolygon
        })
      }
    }

    return [jsonData, outlineData];
  }

  parseInterpData(interpData) { //interpolated data ...
    //FIXME: make this global
    const {min, max, mapping, iter, nCells, k_default, p_default, nMultiples} = this.state;
    const {renderMode, viewMode, idx} = this.props;
    const sources = renderMode['detections'] //FIXME: implement rendering just one or the other interpolation
    var opacity = 150;//170;

    var colorScale = d3.scaleQuantize()
      .domain([0,350])
      //range(["#abdda4", "#e6f598", "#fee08b", "#f46d43", "#d53e4f", "#d53e4f", "#9e0142"]);
      .range(["#d0ebef", "#f9f8c2", "#fed385", "#f88e53", "#dd4030", "#dd4030", "#a50026"]);

    var opaqueColorScale = d3.scaleQuantize() // adjusted for opacity w/ blue background
      .domain([0,350])
      .range(["#c4d2d5", "#d3d9be", "#d4ca92", "#d1936c", "#c05856", "#c05856", "#992f4d"]);

    var data = [];
    var values = [];
    var grid = [];
    if (viewMode == 'smoothed dotmap' || viewMode == 'dotmap'){
      values = this.getInterpFrequencies(interpData);
      if (viewMode == 'smoothed dotmap'){
        grid = this.getSubdividedGrid(interpData);
      }
    }
    else if (viewMode == 'small multiples'){
      values = interpData['smallMultiples'][idx];
      grid = this.getSmallMultiplesGrid(interpData);
    }
    else {
      var default_idx = 0;
      for (var i = 0; i < interpData['results'].length; i++){
        var entry = interpData['results'][i];
        if (entry['source'][0] == renderMode['prediction'][0]){
          if (((entry['source'].length > 1 && entry['source'][1] == renderMode['prediction'][1]) || (entry['source'].length == 1))){
              default_idx = i;
          }
        }
      }
      values = interpData['results'][default_idx]['values']; //default to the selected data source
    }
    for (var i = 0; i < values.length; i++){
      //FIXME!!! need this logic earlier
      if (viewMode == 'smoothed dotmap' || viewMode == 'dotmap'){
        var valueColor = d3.rgb(colorScale(values[i]));
        var color = [valueColor.r, valueColor.g, valueColor.b, opacity];
        if (viewMode == 'smoothed dotmap'){
          var position = [grid['x'][i], grid['y'][i]];
        }
        else {
          var position = [interpData['x'][i], interpData['y'][i]]; //this is different for the dot view!
        }
      }
      else if (viewMode == 'small multiples'){
        var valueColor = d3.rgb(colorScale(values[i]));
        var color = [valueColor.r, valueColor.g, valueColor.b, opacity];
        var position = [grid['x'][i], grid['y'][i]];
      }
      else {
        var valueColor = d3.rgb(colorScale(values[i]));
        var color = [valueColor.r, valueColor.g, valueColor.b, opacity];
        var position = [interpData['x'][i], interpData['y'][i]]; //this is different for the dot view!
      }
      data.push({
        'position': position,
        'color': color,
        'elevation': 0
      });
    }

    var resolution = interpData['resolution'];
    if (viewMode == 'smoothed dotmap'){ //FIXME
      resolution = interpData['resolution']/nCells;
    }
    else if (viewMode == 'small multiples'){
      resolution = interpData['resolution']*3;
    }

    return [data, resolution];
  }

  parseContourData(interpData){
    var jsonData = {
      'type': 'FeatureCollection',
      'features': []
    }
    const {bbox} = this.props;
    const zoom = bbox['zoom'];

    const contours = interpData['contours'];

    //doing a stupid thing: just hard-coding the colors that are appearing in the gridcelllayer so that the contours match
    var colorScale = d3.scaleQuantize()
      .domain([0,350])
      .range(["#d0ebef", "#f9f8c2", "#fed385", "#f88e53", "#dd4030", "#dd4030", "#a50026"]);
      //.range(["#c4d2d5", "#d3d9be", "#d4ca92", "#d1936c", "#c05856", "#c05856", "#992f4d"]);

    var opaqueColorScale = d3.scaleQuantize() // adjusted for opacity w/ blue background
      .domain([0,350])
      .range(["#c4d2d5", "#d3d9be", "#d4ca92", "#d1936c", "#c05856", "#c05856", "#992f4d"]);

    for (var i = 0; i < contours.length; i++){
      if (contours[i]['coordinates'].length > 0){
        if (contours[i]['percentile'] > 0.7 && contours[i]['percentile'] < 0.8){
          var valueColor = d3.rgb(opaqueColorScale(contours[i]['cutoff'] + 10));
          var color = [valueColor.r, valueColor.g, valueColor.b, 255];
          var width = 100;
          var coordinates = contours[i]['coordinates'];
          var line = turf.lineString(coordinates);
          //var curved = turf.bezierSpline(line, {'sharpness': 0.2, 'resolution': 2000});
          // TESTING:
          // first, push on a halo/outline contour
          line['properties'] = {
            'fillColor': color,
            'lineWidth': width
          }
          jsonData.features.push(line); //curved
        }
      }
    }
    return jsonData;
  }

  getSubdividedGrid(interpData){
    //this will be APPROXIMATE! hoping for some small scale action!
    const {nCells} = this.state;
    const {bbox} = this.props;
    const dimension = interpData['dimension'];
    var subX = [];
    var subY = [];
    for (var i = 0; i < interpData['x'].length; i++){
      // find the distance between this grid position and the next one:...
      // for y: if the next y value is less than this one,
      // OR if this is the last index in the list, check the bbox
      var xStep, yStep;
      if (i+1 == interpData['y'].length || interpData['y'][i+1] < interpData['y'][i]){
        var diff = bbox.nwlat - interpData['y'][i];
        yStep = diff/nCells;
      }
      else {
        var diff = interpData['y'][i+1] - interpData['y'][i];
        yStep = diff/nCells;
      }
      // for x: have to check the next "column" -- &, if this is the last column, check against the bbox
      if (i >= interpData['x'].length - dimension){
        var diff = bbox.selng - interpData['x'][i]
        xStep = diff/nCells;
      }
      else {
        var diff = interpData['x'][i+dimension] - interpData['x'][i]
        xStep = diff/nCells;
      }
      for (var y_iter = 0; y_iter < nCells; y_iter++){
        for (var x_iter = 0; x_iter < nCells; x_iter++){
          subY.push(interpData['y'][i] + y_iter * yStep);
          subX.push(interpData['x'][i] + x_iter * xStep)
        }
      }
    }
    return {'x': subX, 'y': subY};
  }

  getSmallMultiplesGrid(interpData){
    var x = [];
    var y = [];
    for (var i = 0; i < interpData['smallMultiplesGridIndices'].length; i++){
      var idx = interpData['smallMultiplesGridIndices'][i];
      x.push(interpData['x'][idx]);
      y.push(interpData['y'][idx]);
    }
    return {'x': x, 'y': y};
  }

  getInterpFrequencies(interpData){ //for dotmaps + kriging
    //FIXME: after IDW, this needs a new approach -- each uncertainty view should show an equivalent amount of information!
    // for each view: sample the small multiples -- each 'supercell' (i.e. nine of the original raster cells) will have nine values
    // for dotmap a: use the subdivided grid to generate 3x3 grid (of 'subcells') for each of 9 original cells;
    // ... randomly order the 9 values within each 3x3 grid
    // for random (b/d): assign each of the original cells in the supercell to one of the nine values
    // for ordered (c): assign each of the original cells in the supercell to one of the nine values, in order
    // ** still need to figure out how to implement the borders

    const { viewMode } = this.props;
    const { nCells } = this.state;
    const nGrid = nCells * nCells;
    const dim = interpData['dimension'];

    var data = []; // this will be returned: a list of values in the same order as the corresponding grid
    var frames = [];

    const indices = interpData['smallMultiplesValueIndices'];
    var multiplier = 1;
    if (viewMode == 'smoothed dotmap'){
      multiplier = 9;    // for the "synthesis dotmap",
    }

    //for each grid cell in interpolation,

    //get an array of the values from the small multiples,
    //then assign these to the appropriate indices in the new 'values' array

    for (var i = 0; i < indices.length; i++){
      var values = [];
      var index = indices[i];
      const gridIndices = [index-dim-1, index-dim, index-dim+1, index-1, index, index+1, index+dim-1, index+dim, index+dim+1]; //indices of the cell in this supercell

      for (var j = 0; j < interpData['smallMultiples'].length; j++){
        for (var m = 0; m < multiplier; m++){
          values.push(interpData['smallMultiples'][j][i]);
        }
      }

      //shuffle, or order, depending on the mode
      // a, b, c = random; d = ordered

      if (viewMode == 'smoothed dotmap'){
        values = this.shuffle(values);
      }

      //then, find out the grid indices corresponding to this 'supercell' index, and assign the values to those
      if (viewMode != 'smoothed dotmap'){
        for (var idx = 0; idx < gridIndices.length; idx++){
          data[gridIndices[idx]] = values[idx];
        }
      }
      else {
        // assign to subdivided grid
        for (var idx = 0; idx < gridIndices.length; idx++){
          for (var elem = 0; elem < nGrid; elem++){
            var subIndex = (gridIndices[idx]*nGrid) + elem
            data[subIndex] = values[idx*nGrid + elem];
          }
        }
      }
    }
    return data;
  }

  shuffle(array){
    var u, v, x;
    for (u = array.length - 1; u > 0; u--) {
        v = Math.floor(Math.random() * (u + 1));
        x = array[u];
        array[u] = array[v];
        array[v] = x;
    }
    return array;
  }

  render() {
    const {viewport, sensorData, interpData, renderMode, bbox, viewMode} = this.props;
    const {mapLayers} = this.state;
    //var viewModeLayer = []; //a single layer is always expected.
    //var sensorLayers = []; //can story multiple layers, but limited to number of sources wihtin a single country.
    var layers = [];
    var opacity = 0.8;

    if (interpData != null && interpData['results'].length > 0 && renderMode['prediction'].length > 0){
      var [data, resolution] = this.parseInterpData(interpData);

      if (viewMode == 'small multiples'){
        layers.push(new GridCellLayer({
          id: 'backdrop-layer',
          cellSize: 0.9*resolution, //FIXME!!
          data: data,
          opacity: 1.0,
          getPosition: d => d.position,
          getColor: d => [179,209,213,255],
          parameters: {
            depthTest: false,
          }
        }));
      }
      layers.push(new GridCellLayer({
          id: 'interpolation-layer',
          cellSize: 0.9*resolution, //FIXME!!
          data: data,
          opacity: opacity,
          getPosition: d => d.position,
          getColor: d => d.color,
          parameters: {
            depthTest: false,
          }
        }));
    }

    if (interpData != null && interpData['contours'] != null && interpData['contours'].length > 0 && viewMode == 'risk contours'){
        var contourData = this.parseContourData(interpData);
        console.log("contours data: ", contourData);
        layers.push(new GeoJsonLayer({
          id: 'contours-layer',
          data: contourData,
          filled: true,
          stroked: false,
          opacity: 0.8,
          lineWidthUnits: 'pixels',
          parameters: {
            depthTest: false,
          },
          getFillColor: d => d['properties']['fillColor'],
          getLineColor: d => d['properties']['fillColor'],
          getLineWidth: 1000 //FIXME
        }));
    }

    if (sensorData != null && sensorData['ID'].length > 0){
      var [jsonData, outlineData] = this.parseSensorData(sensorData);
      console.log('sensor data: ')
      console.log(jsonData);
      for (var i = 0; i < jsonData['features'].length; i++){
        console.log(jsonData['features'][i]);
      }

      layers.push(new GeoJsonLayer({
          id: 'outlines-layer',
          data: outlineData,
          filled: true,
          stroked: false,
          extruded: false,
          getFillColor: [177, 177, 177, 255],
          pointRadiusScale: 2,
          parameters: {
            depthTest: false,
          }
        }));

      layers.push(new GeoJsonLayer({
          id: 'detections-layer',
          data: jsonData,
          filled: true,
          stroked: false,
          extruded: false,
          getFillColor: d => d['geometry']['properties']['fillColor'],
          parameters: {
            depthTest: false,
          }
        }));
    }
    console.log("layers: ", layers);
    console.log("viewport: ", viewport);
    return <DeckGL {...viewport} layers={layers} />
  }
}
