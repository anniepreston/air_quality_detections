import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import {connect} from 'react-redux';
import {updateViewMode, updateSensorDisplay} from './dataActions';
import * as d3 from 'd3';

// import relevant thing from dataActions here

export class ViewMode extends Component {
	constructor(props){
		super(props);
		this.state = {
			height: 0.6*window.innerHeight,
      width: 0.2*window.innerWidth,
      viewModeIndex: 0,
			viewModes: ['interpolation', 'small multiples', 'risk contours', 'dotmap', 'smoothed dotmap'],
			// a = synthesis; b = counting/random/no border, c = counting/random/border, d = counting/ordered/border
      viewGuides: ['interp_view_guide.png', 'smallmultiples_view_guide.png', 'contour_view_guide.png', 'ordered_dotmap_guide.png', 'smoothed_dotmap_guide.png'],
      viewPath: '../view_guides/'
		};

		this.createControls = this.createControls.bind(this);
    this.updateViewMode = this.updateViewMode.bind(this);
	}

	 componentDidMount() {
    	this.createControls();
      const {viewModeIndex} = this.state;
      this.updateViewMode(viewModeIndex);
  	}

  	componentDidUpdate() {
  		this.createControls();
  	}

  	updateViewMode(i){
      this.setState({viewModeIndex: i});
      const {viewModes} = this.state;
      this.props.dispatch(updateViewMode(viewModes[i]));
    }

		updateSensor(){
			const {sensorDisplay} = this.props;
			this.props.dispatch(updateSensorDisplay(!sensorDisplay));
		}

    createControls(){
	  const node = this.node;
  	  d3.select(node).selectAll("*").remove();
  	  const {width, height, viewModes, viewModeIndex, viewPath, viewGuides} = this.state;
			const {sensorDisplay} = this.props;

      const handleViewMode = idx => this.updateViewMode(idx);
			const handleSensor = () => this.updateSensor();
      const xMargin = 4

  	  var container = d3.select(node).append("svg")
  		  .attr("width", width)
  		  .attr("height", height)
  		  .attr("x", 0)
  		  .attr("y", 0);

			container.append("rect")
				.attr("class", "sensorBox")
				.attr("width", 12)
				.attr("height", 12)
				.attr("x", xMargin)
				.attr("y", 12)
				.attr("stroke", "#7a7a7a")
				.attr("fill", "#d7e3f7")
				.attr("fill-opacity", function(d,i){ if (sensorDisplay) return 1; else return 0;})
					.on("click", function(d,i){
						console.log("clickedsensor!");
						handleSensor();
				});

			container.append("text")
				.text("sensors")
				.attr("x", xMargin+18)
				.attr("y", 22)
				.attr("fill", function(d,i){ if (sensorDisplay) return "#7a7a7a"; else return "#c6c6c6";})
				.attr("font-family", "sans-serif")
				.attr("font-size", "14px");
			/*
      container.selectAll("rect .viewModeBox")
        .data(viewModes)
        .enter()
        .append("rect")
        .attr("class", "viewModeBox")
        .attr("width", 12)
        .attr("height", 12)
        .attr("x", xMargin)
        .attr("y", function(d,i){return  12 + i*18; })
        .attr("stroke", "#7a7a7a")
        .attr("fill", "#d7e3f7")
        .attr("fill-opacity", function(d,i){ if (viewModeIndex == i) return 1; else return 0;})
          .on("click", function(d,i){
            handleViewMode(i);
          });

      container.selectAll("rect .viewModeBox")
        .data(viewModes)
        .enter()
        .append("rect")
        .attr("class", "viewModeBox")
        .attr("width", width - xMargin - 30)
        .attr("height", 14)
        .attr("x", xMargin + 18)
        .attr("y", function(d,i){return  11 + i*18; })
        .attr("fill", "#d7e3f7")
        .attr("fill-opacity", function(d,i){ if (viewModeIndex == i) return 1; else return 0;});

      container.selectAll(".viewModeText")
        .data(viewModes)
        .enter()
        .append("text")
        .attr("x", xMargin + 20)
        .attr("y", function(d, i) { return 12 + i*18 + 10; })
        .text(function(d) { return d; })
        .attr("font-weight", function(d, i){

          //if (viewModeIndex == i) return "bold";
          //else return  "normal";

          return "normal";
        })
        .attr("fill", "#7a7a7a")
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px");
			*/

      var imgWidth = width - 2*xMargin;
      var imgHeight = imgWidth*3340/2400;
      if (imgHeight > height-150){
        imgHeight = height-150;
        imgWidth = imgHeight*2400/3340;
      }

      container.append("svg:image")
        .attr("x", xMargin)
        .attr("y", 120)
        .attr("width", imgWidth)
        .attr("height", imgHeight)
        .attr("xlink:href", viewPath + viewGuides[viewModeIndex]);

			d3.select("#viewModeList")
				.selectAll("option")
				.data(viewModes)
				.enter()
				.append("option")
				.text(function(d){return d;})
				.attr("value", function(d,i){return i;});

			d3.select("#viewModeList")
				.on("change", function(i){
					var newViewModeIdx = d3.select(this).property("value");
					handleViewMode(newViewModeIdx);
				});

    }

    render() {
        return (
					<div>
					<div style={{'left': '4px', 'top':'30px', 'position': 'absolute'}}>
						<select id="viewModeList"></select>
					</div>

          <div style={{'width': '100%', 'height': '100%'}} ref={node => this.node = node}/>
					</div>
        );
  	}
}


const mapStateToProps = state => ({
	bbox: state.bbox,
  loading: state.loading,
  error: state.error,
	renderMode: state.renderMode,
	sensorDisplay: state.sensorDisplay
});

export const ViewModeContainer = connect(mapStateToProps)(ViewMode);
