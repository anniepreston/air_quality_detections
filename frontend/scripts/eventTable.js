import React from 'react';
import EventRow from './EventRow.js';

export default class EventTable extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillUnmount() {

    }
    
    shouldComponentUpdate(nextProps) {
        return true;
    }

    render() {
        var rows = [];
        var columnHeaders = null;
        
        if(this.props.events !== null && typeof(this.props.events) !== 'undefined' && this.props.events.length !== 0) {

            columnHeaders = [];

            Object.keys(this.props.events[0]).forEach(function(columnHeader) {
               columnHeaders.push(
                   <th key={columnHeader}>
                        {columnHeader}
                   </th>
               );
            }, this);

            this.props.events.forEach(function(event) {
                rows.push(
                    <EventRow key={event['fire_name']} data-keys={columnHeaders} data={event} setSelectedFire={this.props.setSelectedFire}/>
                );
            }, this);
        }

        return ( 
            <div>
                <table>
                    <tbody>
                        <tr>
                            {columnHeaders}
                        </tr>

                        {rows}

                    </tbody>
                </table>
            </div>
        );
    }
}
