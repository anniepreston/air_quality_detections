function sendApiPostRequest(path, requestBody) {

    return new Promise(function(resolve, reject) {
        if (!requestBody) requestBody = {};

        var request = new XMLHttpRequest();
        request.open('POST', path, true);
        request.setRequestHeader('Content-type', 'application/json');

        console.log("request: ", request);

        request.ontimeout = function() {
        alert('Request timed out at ' + path);
        };

        request.onreadystatechange = function() {
        if (request.readyState != 4) {
        return;
        }

        if (request.status != 200) {
            if (400 <= request.status && request.status <= 499) {
                alert('Bad request (' + request.status + ') for request at ' + path);
                reject(request.status);
            }
            if (500 <= request.status && request.status <= 599) {
                alert('Server error (' + request.status + ') for request at ' + path);
                reject(request.status);
            }

            return;
        }

        console.log('RESPONSE RECEIVED:', JSON.parse(request.responseText));
        resolve(JSON.parse(request.responseText));
        };

        console.log("REQUEST SENT:", JSON.stringify(requestBody), path);
        request.send(JSON.stringify(requestBody));
    });
}

export function postAir(data, callback) {
    return sendApiPostRequest('/air/detections', data);
}

export function postAnimation(data, callback) {
    return sendApiPostRequest('/air/animation', data);
}

//export function postSmallMultiples()

export function postDetectionInterpolation(data, callback) {
    return sendApiPostRequest('/fire/detectionInterpolation', data);
};
