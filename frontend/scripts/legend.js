//legend

import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import * as d3 from 'd3';

export class Legend extends Component {
	constructor(props){
		super(props);
		this.state = {
			height: 0.1*window.innerHeight,
      		width: 0.8*window.innerWidth,
		};

		this.createLegend = this.createLegend.bind(this);
	}

	 componentDidMount() {
    	this.createLegend();
  	}

  	componentDidUpdate() {
  		this.createLegend();
  	}

    createLegend(){
		const node = this.node;
	  	d3.select(node).selectAll("*").remove();
	  	const {width, height} = this.state;

     	 var colorScale = d3.scaleQuantize()
        	.domain([0,350])
        	//.range(["#abdda4", "#e6f598", "#fee08b", "#f46d43", "#d53e4f", "#d53e4f", "#9e0142"]);
        	.range(["#d0ebef", "#f9f8c2", "#fed385", "#f88e53", "#dd4030", "#dd4030", "#a50026"]);
	     
	  	var container = d3.select(node).append("svg")
	  		  .attr("width", width)
	  		  .attr("height", height)
	  		  .attr("x", 0)
	  		  .attr("y", 0);

	    const legendLabels = [
	        {value: 25, label: 'good'},
	        {value: 75, label: 'moderate'},
	        {value: 125, label: 'unhealthy for \n sensitive groups'},
	        {value: 175, label: 'unhealthy'},
	        {value: 250, label: 'very unhealthy'},
	        {value: 400, label: 'hazardous'}
	      ];  

	      const legendData = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500];
	      const xMargin = 4;
	      const legendWidth = 0.95*(width - xMargin);
	      const ratio = 500/legendWidth;

		/*
	      container.append("text")
	        .text("AQI")
	        .attr("font-family", "sans-serif")
	        .attr("font-size", "12px")
	        .attr("fill", "#7a7a7a")
	        .attr("x", 20)
	        .attr("y", 20);
	        */

	      container.selectAll('.legend')
	          .data(d3.range(legendWidth), function(d) { return d; })
	        .enter().append("rect")
	          .attr("class", "bars")
	          .attr("x", function(d, i) { return i + xMargin; })
	          .attr("y", 12)
	          .attr("height", 14)
	          .attr("width", 1)
	          .style("fill", function(d) { return colorScale(d*ratio); })

	      container.selectAll('.legendText')
	        .data(legendLabels)
	        .enter()
	        .append("text")
	        .attr("x", function(d) { return xMargin + (d.value/ratio); })
	        .attr("y", 36)
	        .attr("fill", "#7a7a7a")
	        .attr("text-anchor", "middle")
	        .text(function(d) { return d.label; })
	        .attr("font-family", "sans-serif")
	        .attr("font-size", "10px");

	      container.selectAll('.legendNumber')
	        .data(legendData)
	        .enter()
	        .append("text")
	        .attr("x", function(d) {return xMargin + d/ratio; })
	        .attr("y", 8)
	        .attr("fill", "#c6c6c6")
	        .text(function(d) { return d; })
	        .attr("font-family", "sans-serif")
	        .attr("font-size", "10px")
	        .attr("text-anchor", "middle");
    }

    render() {
        return (
          <div style={{'width': '100%', 'height': '100%'}} ref={node => this.node = node}> 
          </div>
        );
  	}
}
