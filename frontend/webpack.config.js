// NOTE: To use this example standalone (e.g. outside of deck.gl repo)
// delete the local development overrides at the bottom of this file

// avoid destructuring for older Node version support
const resolve = require('path').resolve;
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const CONFIG = {
  entry: [
    //Error: webpack-dev-server 404 not found error in developer tools
    //Solution reference: https://stackoverflow.com/questions/41342144/webpack-hmr-webpack-hmr-404-not-found
    //'webpack-hot-middleware/client?http://localhost:8000', // webpack dev server host and port
    resolve('./scripts/app.js')
  ],

  devtool: 'source-map',

  module: {
    rules: [
      {
        // Compile ES2015 using babel
        test: /\.js$/,
        loader: 'babel-loader',
        include: [resolve('.')],
        exclude: [/node_modules/],
      },{
        // Compile ES2015 using babel
        test: /\.jsx$/,
        loader: 'babel-loader',
        include: [resolve('.')],
        exclude: [/node_modules/],
      },{
        test: /\.css$/,
        use: [
	     {
	       loader: MiniCssExtractPlugin.loader,
      },
	'css-loader',
	],
    }

    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './scripts/index.html'
    }),
    new webpack.ProvidePlugin ({
      ReactDOM: 'react-dom',
      React: 'react',
      $: 'jquery',
      jQuery: 'jquery'
    }),
    new MiniCssExtractPlugin({
	filename: 'styles.css'
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    //new webpack.optimize.UglifyJsPlugin()
    ],

  resolve: {
    alias: {
      // From mapbox-gl-js README. Required for non-browserify bundlers (e.g. webpack):
      'mapbox-gl$': resolve('./node_modules/mapbox-gl/dist/mapbox-gl.js')
    },
    extensions: ['.js', '.jsx', '.css']
  },

  output: {
    filename: "bundle.js"
  }
};

// This line enables bundling against src in this repo rather than installed deck.gl module
module.exports = env => (env ? require('../webpack.config.local')(CONFIG)(env) : CONFIG);
