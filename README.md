# README #

uncertainty-aware vis for air quality data
 
~ ~ ~ 

requirements:

python-aqi https://pypi.org/project/python-aqi/
pyKrige https://github.com/bsmurphy/PyKrige
 
~ ~ ~
 
React+Redux+DeckGL

* * * *

cd frontend

npm install

npm run build

~ ~ ~

cd backend

gunicorn backend:app --reload

~ ~ ~

localhost:8000/index.html

