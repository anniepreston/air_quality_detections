import falcon
from pdb import set_trace as bp
import json
import psycopg2.extras
from urllib.request import urlopen
import sklearn
import math

from fireCluster import clusterDetections
from fireInterpolate import interpolatePoints, interpolateValues
from perimeterInterpolate import interpolatePerimeter

#TODO: separate these into individual files !!!!

class FirePerimetersResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb

    def on_post(self, req, resp):
        requestBody = json.loads(req.stream.read().decode('utf-8'))

        state = requestBody.get('state')
        name = requestBody.get('fire_name')

        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        # TODO is there a better way to build these kinds of queries? maybe just have 2 separate conditions...
        queryString = """
        SELECT state, 
            fire_name, 
            date,
            acres,
            ST_AsGeoJSON(centroid) as centroid,
            ST_AsGeoJson(perimeter) as perimeter 
                FROM fire_perimeter 
                    WHERE state=%s 
                    AND fire_name=%s 
                        ORDER BY date ASC;
        """

        query = cursor.mogrify(queryString, tuple([state, name]))

        print(query.decode('utf-8'))

        cursor.execute(query)

        queryResult = cursor.fetchall()

        for result in queryResult:
            result['centroid'] = json.loads(result['centroid'])
            result['perimeter'] = json.loads(result['perimeter'])

        responseDict = queryResult

        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)

class FireRadiativePowerResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb
    
    def on_post(self, req, resp):
        requestBody = json.loads(req.stream.read().decode('utf-8'))
        
        distance = requestBody.get('distance')
        originCentroid = requestBody.get('origin_centroid')
        startDate = requestBody.get('start_date')
        endDate = requestBody.get('end_date')
        
        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        
        #queryString = """
        #SELECT ST_Distance(ST_GeomFromGeoJSON(%s)::geography, frp.centroid::geography) as distance,
        #    ST_AsGeoJSON(frp.centroid) as centroid,
        #    ST_AsGeoJSON(frp.footprint) as footprint
        #        FROM fire_detection AS frp
        #            WHERE ST_Distance(ST_GeomFromGeoJSON(%s)::geography, frp.centroid::geography) < %s;
        #"""	
        
        queryString = """
            SELECT date,
            confidence,
            power,
            sensor,
            source,
            ST_AsGeoJSON(frp.centroid) as centroid,
            ST_AsGeoJSON(frp.footprint) as footprint
            FROM fire_radiative_power AS frp
            WHERE %s < date
            AND date < %s
            AND ST_DWithin(frp.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
            """
        
        query = cursor.mogrify(queryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))
        
        print(query.decode('utf-8'))
            
        cursor.execute(query)       
        frp = cursor.fetchall()
        
        for frpPoint in frp:
            frpPoint['centroid'] = json.loads(frpPoint['centroid'])
            frpPoint['footprint'] = json.loads(frpPoint['footprint'])
    
        clusters = clusterDetections(frp)
        interpolation = interpolatePoints(frp, clusters)

        #interpolations = {'points': interpolation['points'].tolist(), 'values': interpolation['values'].tolist()}
        interpolations = {'points': interpolation['points'], 'values': interpolation['values']}

        responseDict = interpolations
        
        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)

class FireDetectionResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb
    
    def on_post(self, req, resp):
        requestBody = json.loads(req.stream.read().decode('utf-8'))
        
        distance = requestBody.get('distance')
        originCentroid = requestBody.get('origin_centroid')
        startDate = requestBody.get('start_date')
        endDate = requestBody.get('end_date')
        
        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        
        queryString = """
            SELECT date,
            instantaneous_size,
            instantaneous_temperature,
            confidence,
            sensor,
            ST_AsGeoJSON(det.centroid) as centroid,
            ST_AsGeoJSON(det.footprint) as footprint
            FROM fire_detection AS det
            WHERE %s < date
            AND date < %s
            AND ST_DWithin(det.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
            """
        
        query = cursor.mogrify(queryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))
        
        print(query.decode('utf-8'))
        
        cursor.execute(query)
        
        queryResult = cursor.fetchall()
        
        # TODO figure out a better way to do this?
        for result in queryResult:
            result['centroid'] = json.loads(result['centroid'])
            result['footprint'] = json.loads(result['footprint'])
    
        responseDict = queryResult
        
        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)

class DetectionAndRadiativePowerResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb
    
    def on_post(self, req, resp):
        requestBody = json.loads(req.stream.read().decode('utf-8'))
        
        distance = requestBody.get('distance')
        originCentroid = requestBody.get('origin_centroid')
        startDate = requestBody.get('start_date')
        endDate = requestBody.get('end_date')
        
        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        
        queryString = """
            SELECT date,
            instantaneous_size,
            instantaneous_temperature,
            confidence,
            sensor,
            ST_AsGeoJSON(det.centroid) as centroid,
            ST_AsGeoJSON(det.footprint) as footprint
            FROM fire_detection AS det
            WHERE %s < date
            AND date < %s
            AND ST_DWithin(det.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
            """
        
        query = cursor.mogrify(queryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))
        
        print(query.decode('utf-8'))
        
        cursor.execute(query)
        
        detections = cursor.fetchall()
        
        # TODO figure out a better way to do this?
        for result in detections:
            result['centroid'] = json.loads(result['centroid'])
            result['footprint'] = json.loads(result['footprint'])
            
        queryString = """
            SELECT date,
            confidence,
            power,
            sensor,
            source,
            ST_AsGeoJSON(frp.centroid) as centroid,
            ST_AsGeoJSON(frp.footprint) as footprint
            FROM fire_radiative_power AS frp
            WHERE %s < date
            AND date < %s
            AND ST_DWithin(frp.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
            """
        
        query = cursor.mogrify(queryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))
        
        print(query.decode('utf-8'))
            
        cursor.execute(query)       
        frp = cursor.fetchall()
        
        for frpPoint in frp:
            frpPoint['centroid'] = json.loads(frpPoint['centroid'])
            frpPoint['footprint'] = json.loads(frpPoint['footprint'])

        responseDict = {}
        responseDict['frp'] = frp
        responseDict['detections'] = detections
        
        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)

class DetectionInterpolationResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb

    def on_post(self, req, resp):
        requestBody = json.loads(req.stream.read().decode('utf-8'))
        
        distance = requestBody.get('distance')
        originCentroid = requestBody.get('origin_centroid')
        startDate = requestBody.get('start_date')
        endDate = requestBody.get('end_date')

        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        frpQueryString = """
            SELECT date,
            confidence,
            power,
            sensor,
            source,
            ST_AsGeoJSON(frp.centroid) as centroid,
            ST_AsGeoJSON(frp.footprint) as footprint
            FROM fire_radiative_power AS frp
            WHERE %s < date
            AND date < %s
            AND ST_DWithin(frp.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
            """
                
        query = cursor.mogrify(frpQueryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))

        print(query.decode('utf-8'))
        
        cursor.execute(query)       
        frp = cursor.fetchall()

        queryString = """
            SELECT date,
            instantaneous_size,
            instantaneous_temperature,
            confidence,
            sensor,
            ST_AsGeoJSON(det.centroid) as centroid,
            ST_AsGeoJSON(det.footprint) as footprint
            FROM fire_detection AS det
            WHERE %s < date
            AND date < %s
            AND ST_DWithin(det.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
            """
        
        query = cursor.mogrify(queryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))
        
        print(query.decode('utf-8'))
        
        cursor.execute(query)
        
        detections = cursor.fetchall()
        
        # TODO figure out a better way to do this?
        for result in detections:
            result['centroid'] = json.loads(result['centroid'])
            result['footprint'] = json.loads(result['footprint'])
        
        for frpPoint in frp:
            frpPoint['centroid'] = json.loads(frpPoint['centroid'])
            frpPoint['footprint'] = json.loads(frpPoint['footprint'])
    
        clusters = clusterDetections(frp, detections) #FIXME
        interpolation = interpolatePoints(frp, detections, clusters)

        #interpolations = {'points': interpolation['points'].tolist(), 'values': interpolation['values'].tolist()}
        interpolations = {'points': interpolation['points'], 'values': interpolation['values']}

        responseDict = interpolations
        
        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)

class FireListResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb

    def on_post(self, req, resp):
        requestBody = json.loads(req.stream.read().decode('utf-8'))

        state = requestBody.get('state')

        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        queryString = """
        SELECT fire_name, 
            COUNT(*) AS num_perimeters, 
            MAX(acres) AS max_area,
            MIN(date) AS range_min, 
            MAX(date) AS range_max
                FROM fire_perimeter 
                    WHERE state=%s GROUP BY fire_name 
                        ORDER BY max(acres) DESC; 
        """

        query = cursor.mogrify(queryString, tuple([state]))

        print(query.decode('utf-8'))

        cursor.execute(query);

        queryResult = cursor.fetchall()

        responseDict = queryResult

        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)

class FireInterpolationResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb

    def on_post(self, req, resp):
        # request body should specify the target fire (fire name) and target date to interpolate towards
        requestBody = json.loads(req.stream.read().decode('utf-8'))

        state = requestBody.get('state')
        fire_name = requestBody.get('fire_name')
        start_date = requestBody.get('start_date')
        end_date = requestBody.get('end_date')
        perimeter_date = requestBody.get('perimeter_date')
        
        nGrid = requestBody.get('nGrid')
        if nGrid is not None:
            nGrid = int(nGrid)

        bandwidth = requestBody.get('bandwidth')
        if bandwidth is not None:
            bandwidth = float(bandwidth)

        perimeterMultiplier = requestBody.get('perimeterMultiplier')
        if perimeterMultiplier is not None:
            perimeterMultiplier = float(perimeterMultiplier)

        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        # TODO is there a better way to build these kinds of queries? maybe just have 2 separate conditions...
        perimeterQueryString = """
        SELECT state, 
            fire_name, 
            fire_number, 
            description, 
            unit_identifier,
            acres,
            agency,
            comments,
            date,
            ST_AsGeoJson(centroid) as centroid, 
            ST_AsGeoJson(perimeter) as perimeter
                FROM fire_perimeter 
                    WHERE date = %s 
                    AND state=%s 
                    AND fire_name=%s;
        """

        perimeterQuery = cursor.mogrify(perimeterQueryString, tuple([perimeter_date, state, fire_name]))
        print(perimeterQuery.decode('utf-8'))
        cursor.execute(perimeterQuery)
        perimeter = cursor.fetchone()
        # TODO figure out a better way to do this? 

        perimeter['centroid'] = json.loads(perimeter['centroid'])
        perimeter['perimeter'] = json.loads(perimeter['perimeter'])

        # distance to query points from the perimeter... based on the area of the perimeter times some fudge factor to grab 
        # more-or-less all of the relevant, available points
        distance = perimeterMultiplier * math.sqrt(4046.86 * perimeter['acres']); 
        originCentroid = perimeter['centroid']

        detectionQueryString = """
        SELECT date,
            instantaneous_size, 
            instantaneous_temperature,
            confidence,
            sensor,
            ST_AsGeoJSON(det.centroid) as centroid, 
            ST_AsGeoJSON(det.footprint) as footprint
                FROM fire_detection AS det
                    WHERE %s < date 
                    AND date < %s
                    AND ST_DWithin(det.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
        """

        detectionQuery = cursor.mogrify(detectionQueryString, tuple([perimeter_date, target_date, json.dumps(originCentroid), distance]))
        print(detectionQuery.decode('utf-8'))
        cursor.execute(detectionQuery)
        detections = cursor.fetchall()

        # TODO figure out a better way to do this? 
        for detection in detections:
            detection['centroid'] = json.loads(detection['centroid'])
            detection['footprint'] = json.loads(detection['footprint'])

        frpQueryString = """
        SELECT date,
            confidence,
            power,
            sensor,
            source,
            ST_AsGeoJSON(frp.centroid) as centroid,
            ST_AsGeoJSON(frp.footprint) as footprint
                FROM fire_radiative_power AS frp
                    WHERE %s < date 
                    AND date < %s
                    AND ST_DWithin(frp.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
        """

        frpQuery = cursor.mogrify(frpQueryString, tuple([perimeter_date, target_date, json.dumps(originCentroid), distance]))
        print(frpQuery.decode('utf-8'))
        cursor.execute(frpQuery)
        frp = cursor.fetchall()

        # TODO figure out a better way to do this? 
        for frpPoint in frp:
            frpPoint['centroid'] = json.loads(frpPoint['centroid'])
            frpPoint['footprint'] = json.loads(frpPoint['footprint'])

        interpolatedPerimeter = interpolatePerimeter(perimeter, detections, frp, originCentroid, distance, nGrid = nGrid, bandwidth=bandwidth, perimeterMultiplier = perimeterMultiplier)

        print('Finished interpolating...')

        responseDict = interpolatedPerimeter
        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)

class TargetPerimeterResource():
    def __init__(self, pgdb):
        self.pgdb = pgdb

    def on_post(self, req, resp):
        # request body should specify the target fire (fire name) and target date to interpolate towards
        requestBody = json.loads(req.stream.read().decode('utf-8'))
        
        distance = requestBody.get('distance')
        originCentroid = requestBody.get('origin_centroid')
        startDate = requestBody.get('start_date')
        endDate = requestBody.get('end_date')
        state = 'CA' #FIXME
        perimeter = requestBody.get('perimeter')
        cursor = self.pgdb.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        detectionQueryString = """
        SELECT date,
            instantaneous_size, 
            instantaneous_temperature,
            confidence,
            sensor,
            ST_AsGeoJSON(det.centroid) as centroid, 
            ST_AsGeoJSON(det.footprint) as footprint
                FROM fire_detection AS det
                    WHERE %s < date 
                    AND date < %s
                    AND ST_DWithin(det.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
        """

        detectionQuery = cursor.mogrify(detectionQueryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))
        print(detectionQuery.decode('utf-8'))
        cursor.execute(detectionQuery)
        detections = cursor.fetchall()

        # TODO figure out a better way to do this? 
        for detection in detections:
            detection['centroid'] = json.loads(detection['centroid'])
            detection['footprint'] = json.loads(detection['footprint'])

        print('detections: ', detections)

        frpQueryString = """
        SELECT date,
            confidence,
            power,
            sensor,
            source,
            ST_AsGeoJSON(frp.centroid) as centroid,
            ST_AsGeoJSON(frp.footprint) as footprint
                FROM fire_radiative_power AS frp
                    WHERE %s < date 
                    AND date < %s
                    AND ST_DWithin(frp.centroid::geography, ST_GeomFromGeoJSON(%s)::geography, %s)
        """

        frpQuery = cursor.mogrify(frpQueryString, tuple([startDate, endDate, json.dumps(originCentroid), distance]))
        print(frpQuery.decode('utf-8'))
        cursor.execute(frpQuery)
        frp = cursor.fetchall()

        # TODO figure out a better way to do this? 
        for frpPoint in frp:
            frpPoint['centroid'] = json.loads(frpPoint['centroid'])
            frpPoint['footprint'] = json.loads(frpPoint['footprint'])

        interpolatedPerimeter = interpolatePerimeter(perimeter, detections, frp, originCentroid, distance, nGrid = 400, bandwidth=0.004, perimeterMultiplier = 1.75)

        print('Finished interpolating...')

        responseDict = interpolatedPerimeter
        resp.body = json.dumps(responseDict, separators=(',', ':'), allow_nan = False, default=str)
