import numpy as np
import pykrige.kriging_tools as kt
from pykrige.ok import OrdinaryKriging
from pykrige.uk import UniversalKriging
import shapely.geometry
import pyproj
import math
from scipy.stats import norm
from skimage import measure
import json

#documentation: https://github.com/bsmurphy/PyKrige

def ordinaryKrige(sourceList, sensorData, bbox): # is this the best way to deal with the bounding box?
	# generate a grid: squares with a constant size in meters (!)
	# (because of deck.gl layer requirements)
	print('performing kriging for sources: ', sourceList)
	lonLatGrid = getGrid(bbox)
	x_adjusted = [x % 360 for x in lonLatGrid['x_centers']]

	# set up data structure for data to return, including small multiples grids
	interpData = {
		'x': lonLatGrid['x_grid'], #coordinates of upper left of cell
		'y': lonLatGrid['y_grid'],
		'resolution': lonLatGrid['resolution'],
		'dimension': lonLatGrid['dimension'],
		'results': [],
		'smallMultiples': [],
		'smallMultiplesValueIndices' : [],
		'smallMultiplesGridIndices': [],
		'contours': []
	}

	print('...for sources: ', sourceList)
	# perform kriging for whichever data sources are requested

	values = np.array([x for ind, x in enumerate(sensorData['aqi']) if sensorData['source'][ind] in sourceList]).astype(float)
	#values = np.array(sensorData['aqi'])

	# get the lon/lat coordinates in the format that the kriging package wants
	lon = [l % 360 for ind, l in enumerate(sensorData['lon']) if sensorData['source'][ind] in sourceList] # i.e., x position; convert from [-180, 180] to [0, 360] longitude coordinates
	lat = [l for ind, l in enumerate(sensorData['lat']) if sensorData['source'][ind] in sourceList] # i.e., y position
	#lon = [l % 360 for l in sensorData['lon']]
	#lat = sensorData['lat']

	OK = OrdinaryKriging(lon, lat, values, variogram_model='spherical', nlags=10, coordinates_type='geographic')
	z, ss = OK.execute('points', x_adjusted, lonLatGrid['y_centers'])
	#kt.write_asc_grid(grid['x_grid'], grid['y_grid'], z, filename="output.asc")

	# store grid of kriging estimates and standard deviations
	results = {
		'source': sourceList,
		'values': z.tolist(),
		'stddev': [math.sqrt(math.fabs(ss_val)) for ss_val in ss.tolist()]
	}
	interpData['results'].append(results)

	# now, use the kriging estimates to get the small multiples grids
	indices = getSmallMultiplesIndices(len(results['values']), lonLatGrid['dimension'])
	interpData['smallMultiplesGridIndices'] = indices['gridIndices']
	interpData['smallMultiplesValueIndices'] = indices['valueIndices']
	interpData['smallMultiples'] = getSmallMultiples(results, indices['valueIndices'])
	interpData['contours'] = getContours(results, lonLatGrid) # the original lon-lat, for drawing contours

	return interpData

def getContours(results, lonLatGrid):
	# FIXME!! need to check for edge cases. (literally: at the edge of the map, is the value at the higher percentile different from the mean?)

	cutoffs = [50, 100, 150, 200, 300]
	contours = []

	longitudes = lonLatGrid['x_centers']
	latitudes = lonLatGrid['y_centers']
	values = results['values']
	s = results['stddev']

	lonArray = np.reshape(longitudes, (-1, lonLatGrid['dimension']))
	latArray = np.reshape(latitudes, (-1, lonLatGrid['dimension']))

	# generate frames 5, 6, 7, 8
	percentiles = [0.75]#[5.5/9, 6.5/9, 7.5/9, 8.5/9]
	for p in percentiles:
		frame = []
		for i in range(len(values)):
			score = norm.ppf(p, loc=values[i], scale=s[i])
			frame.append(np.amax([score, 0.0]))
		frameArray = np.reshape(frame, (-1, lonLatGrid['dimension']))
		for c in cutoffs:
			rawContours = measure.find_contours(frameArray, c)
			# deal with interpolated positions: for now, just round to nearest
			for rc in rawContours:
				contoursLonLat = []
				for pair in rc.tolist():
					lon = lonArray[int(np.round(pair[0]))][int(np.round(pair[1]))]
					lat = latArray[int(np.round(pair[0]))][int(np.round(pair[1]))]
					contoursLonLat.append([lon, lat])
				contours.append({
					'percentile': p,
					'cutoff': c,
					'coordinates': contoursLonLat
				})

	return contours

def getSmallMultiples(results, indices):
	n_multiples = 9
	std_dev = results['stddev']
	z = results['values']

	values = []

	# for each small multiple,
	for n in range(n_multiples):
		# get the z-score (?) for this quantile:
		multiple = []
		percentile = (n + 0.5)/float(n_multiples)

		for i in indices:
			score = norm.ppf(percentile, loc=z[i], scale=std_dev[i])
			multiple.append(np.amax([score, 0.0]))

		values.append(multiple)
	return values

def getSmallMultiplesIndices(n_values, dim):
	indices = {
		'gridIndices': [],
		'valueIndices': []
	}
	n_columns = math.floor((n_values/float(dim))/3.)
	for j in range(n_columns):
		i = 2 + (dim * j * 3)
		while i < ((j*3)+1)*int(dim)-1:
			indices['gridIndices'].append(i) # top left
			indices['valueIndices'].append(i-1 + int(dim)) # center of 3x3 cell
			i += 3
	return indices

"""
def universalKrige(sensorData, bbox): # is this the best way to deal with the bounding box?
	data = sensorData['aqi']
	lon = [l % 360 for l in sensorData['lon']] # i.e., x position; convert from [-180, 180] to [0, 360] longitude coordinates
	lat = sensorData['lat'] # i.e., y position

	grid = getGrid(bbox)

	UK = UniversalKriging(lon, lat, data, variogram_model='gaussian', verbose=True)

	#convert the x grid to OK-friendly coords:
	x_adjusted = [x % 360 for x in grid['x_centers']]

	z, ss = UK.execute('points', x_adjusted, grid['y_centers'])
	#kt.write_asc_grid(grid['x_grid'], grid['y_grid'], z, filename="output.asc")

	interpData = {
		'values': z.tolist(),
		'sigmas': ss.tolist(),
		'x': grid['x_grid'],
		'y': grid['y_grid']
	}

	return interpData
"""

def getResolution(s, e):
	#FIXME! make this adjustable!
	resolution = 5*math.sqrt(abs(e[1]-s[1])*abs(e[0]-s[0])/70000.)
	#resolution = math.sqrt(abs(e[1]-s[1])*abs(e[0]-s[0])/70000.)
	return resolution

def getGrid(bbox):
	# TEMP/FIXME: capability to save and reload a grid
	write_grid = False
	read_grid = True

	if read_grid == False:

		# given lon/lat bounds,
		# return lon/lat coordinates of an evenly spaced grid within the bounds
		# ...assistance from here: https://stackoverflow.com/questions/40342355/how-can-i-generate-a-regular-geographic-grid-using-python

		#FIXME: how to make this way faster?? --> precompute

		print("bbox: ", bbox)
		# define projections:
		p_ll = pyproj.Proj(init='epsg:4326') # lon/lat
		p_mt = pyproj.Proj(init='epsg:3857') # metric; same as EPSG:900913

		# Create corners of rectangle to be transformed to a grid
		nw = shapely.geometry.Point((bbox['nwlng'], bbox['nwlat']))
		se = shapely.geometry.Point((bbox['selng'], bbox['selat']))

		# Project corners to target projection
		s = pyproj.transform(p_ll, p_mt, nw.x, nw.y) # Transform NW point to 3857
		e = pyproj.transform(p_ll, p_mt, se.x, se.y) # .. same for SE

		print("s: ", s)
		print("e: ", e)

		#FIXME: for now, fixed resolution...
		stepsize = getResolution(s, e) # m grid step size
		print("stepsize: ", stepsize)

		# Iterate over 2D area
		#for the deck.gl layer, we need the top left coords of these cells
		x_grid = []
		y_grid = []
		x_centers = []
		y_centers = []
		x = s[0]
		dim = 0
		while x < e[0]:
		    dimension = dim
		    dim = 0
		    y = e[1]
		    while y < s[1]:
		        p = shapely.geometry.Point(pyproj.transform(p_mt, p_ll, x, y))
		        p_center = shapely.geometry.Point(pyproj.transform(p_mt, p_ll, x + 0.5*stepsize, y + 0.5*stepsize))
		        x_grid.append(p.x)
		        y_grid.append(p.y)
		        x_centers.append(p_center.x)
		        y_centers.append(p_center.y)
		        y += stepsize
		        dim += 1
		    x += stepsize

		print("finished calculating grid")
		grid = ({'x_grid': x_grid, 'y_grid': y_grid, 'x_centers': x_centers, 'y_centers': y_centers, 'resolution': stepsize, 'dimension': dimension})

		if write_grid == True:
			with open("grid.json", "w") as file:
				json.dump(grid, file)
	if read_grid == True:
		with open("taiwan_grid.json") as file:
			grid = json.load(file)

	return grid
