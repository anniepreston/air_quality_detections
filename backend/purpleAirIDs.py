def getAreaData(sensorData, nwlat, selat, nwlng, selng):
	path =  "https://www.purpleair.com/data.json?fetch=true&nwlat=" + nwlat + "&selat=" + selat + "&nwlng=" + nwlng + "&selng=" + selng
        
	with urlopen(path) as rawData:
		string = rawData.read().decode('utf-8')
    
	result = json.loads(string, parse_float=decimal.Decimal)
	fields = result['fields']
	data = result['data']

	for row in data:
		if str(row[fields.index('pm')]) != 'None':
			sensorData['source'].append('PurpleAir')
			sensorData['ID'].append(row[fields.index('ID')])
			sensorData['lon'].append(row[fields.index('Lon')])
			sensorData['lat'].append(row[fields.index('Lat')])
			sensorData['pm25'].append(row[fields.index('pm')])
			sensorData['aqi'].append(aqi.to_iaqi(aqi.POLLUTANT_PM25, str(row[fields.index('pm')]), algo=aqi.ALGO_EPA))


def getSensorData(sensorData):
	sensorIDs = sensorData['ID']

	if type(sensorIDs) is int:
		sensorIDs = [sensorIDs]

	sensorData = {key: [] for key in sensorData} #clear out the current (low-res) values FIXME: make sure this isn't clearing the AirNow values

	path = "https://www.purpleair.com/json?show="

	if type(sensorIDs) is list:
		length = len(sensorIDs)

	for ID in sensorIDs:
		with urlopen(path + str(ID)) as rawData:
			string = rawData.read().decode('utf-8')
			data = json.loads(string)['results'][0] #FIXME: this actually returns two results, one from each 'side' of the sensor...
			if data['DEVICE_LOCATIONTYPE'] == 'outside': #filter for OUTDOORS sensors only
				sensorData['source'].append('PurpleAir')
				sensorData['ID'].append(data['ID'])
				sensorData['lon'].append(data['Lon'])
				sensorData['lat'].append(data['Lat'])
				sensorData['pm25'].append(data['PM2_5Value']) #FIXME: can add more sophisticated info here
				sensorData['aqi'].append(aqi.to_iaqi(aqi.POLLUTANT_PM25, str(data['PM2_5Value']), algo=aqi.ALGO_EPA))

# goal: list of known IDs that are definitely outdoors!

# rough bounds for US:
bounds = {
	'nwlat': '49.0',
	'nwlng': '-119.0',
	'selat': '32.0',
	'selng': '-115.0'
}