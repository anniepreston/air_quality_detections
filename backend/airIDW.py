import numpy as np
import idw
from airKrige import getGrid
import math

def IDW(sourceList, sensorData, bbox, k=[10], p=[5]):
	# perform IDW on the sensor data; return a grid of estimated values

	lonLatGrid = getGrid(bbox) #returns: x_grid, y_grid, each = a list of coords (1x per coord) in the grid
	cartesianGrid = []

	for i in range(len(lonLatGrid['x_centers'])):
		cartesianGrid.append(toCartesian(lonLatGrid['x_centers'][i], lonLatGrid['y_centers'][i]))
	cartesianGrid = np.array(cartesianGrid).astype(float)

	interpData = {
		'x': lonLatGrid['x_grid'], #coordinates of upper left of cell
		'y': lonLatGrid['y_grid'],
		'resolution': lonLatGrid['resolution'],
		'dimension': lonLatGrid['dimension'],
		'results': []
	}

	#FIXME: be careful about which values we're interpolating (aqi vs. pm)
	#values = np.array(sensorData['aqi'])
	values = np.array([x for ind, x in enumerate(sensorData['aqi']) if sensorData['source'][ind] in sourceList]).astype(float) 

	# convert (lon, lat) positions to 3D cartesian coordinates
	coords = []
	j = 0
	for i in range(len(sensorData['lon'])):
		if sensorData['source'][i] in sourceList:
			coords.append(toCartesian(sensorData['lon'][i], sensorData['lat'][i]))
			j += 1

	coords = np.array(coords).astype(float)
	print("coords shape: ", coords.shape)
	print("values shape: ", values.shape)

	if len(values) > 0:
		idwTree = idw.tree(coords, values) #make one of these trees per data combination

		for k_val in k:
			for p_val in p:
				predictions = idwTree(cartesianGrid, k=k_val, p=p_val)
				results = {
					'source': sensorData['source'],
					'p': p_val,
					'k': k_val,
					'values': predictions.tolist()
				};
				interpData['results'].append(results)

	return interpData

def toCartesian(lon, lat):
	earthRadius = 6371000 # AVERAGE earth radius in m
	x = earthRadius * math.cos(math.radians(lat)) * math.cos(math.radians(lon))
	y = earthRadius * math.cos(math.radians(lat)) * math.sin(math.radians(lon))
	z = earthRadius * math.sin(math.radians(lat))

	return [x, y, z]
