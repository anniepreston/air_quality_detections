import json
from urllib.request import urlopen
import sklearn
import math
from decimal import * # for parsing floats into json
import aqi # conversion between pollutant concentration and AQI
import csv
import pandas as pd
import requests

#python scripts:
from airKrige import ordinaryKrige #, universalKrige
from airIDW import IDW

debug_mode = False # in debug mode, we load pre-fetched data; otherwise, we query the sites for real-time/historical data

def getOutsideIDs():
	outsideIDs = []
	with open('purpleair_outside_ids.csv', 'r') as csvfile: #NOTE: this file doesn't necessarily represent the most recent or inclusive possible list
		reader = csv.reader(csvfile)
		for row in reader:
			for item in row:
				outsideIDs.append(int(item))
	return outsideIDs

def getThingspeakIDs():
	IDs = {}
	with open('thingspeakIDs.csv', 'r') as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			IDs[row['purpleAirID']] = {'thingspeakID': row['thingspeakID'], 'key': row['thingspeakKey']}
	return IDs

def checkSensorOutside(ID, outsideIDs):
	outside = False
	if ID in outsideIDs:
		outside = True
	return outside

class AirSensorResource():
# return data for all PURPLEAIR sensors in the specified area
	def on_post(self, req, resp):
		if debug_mode == True:
			with open('scenario_d.txt') as json_file: # load pre-processed data
				data = json.load(json_file)
				resp.body = json.dumps(data, separators=(',', ':'), allow_nan = False, default=str)

		else: # load data from the internet, and if specified below, download that data formatted correctly to read in debug mode
			requestBody = json.loads(req.stream.read().decode('utf-8'))
			sourceList = requestBody.get('sourceList')
			nwlat = requestBody.get('nwlat')
			selat = requestBody.get('selat')
			nwlng = requestBody.get('nwlng')
			selng = requestBody.get('selng')
			zoom = requestBody.get('zoom')
			res = requestBody.get('resolution')
			method = requestBody.get('predictionMethod')
			date = requestBody.get('date')

			#FIXME: temporary for the demo. Related to the hover stations
			if 'TaiwanEPA' in sourceList:
				sensorData = {
					'source': [],
					'ID': [],
					'station': [],
					'pm25': [],
					'lon': [],
					'lat': [],
					'aqi': [],
				}
			else:
				sensorData = {
					'source': [],
					'ID': [],
					'pm25': [],
					'lon': [],
					'lat': [],
					'aqi': [],
				}

			print("source list: ", sourceList)
			print("method: ", method)


			if 'PurpleAir' in sourceList:
				if date != "" and date is not None:
				# if a date is specified, fetch historical data from the ThingSpeak server
					getThingSpeakData(sensorData, nwlat, selat, nwlng, selng, date)
				else:
				# in U.S. mode, always retrieve the purpleair data
					getAreaData(sensorData, nwlat, selat, nwlng, selng)

			if 'AirNow' in sourceList:
				getAirNowData(sensorData, nwlat, selat, nwlng, selng, date)
			if 'TaiwanEPA' in sourceList:
				if date != "" and date is not None: #FIXME: want to detect whether date is current or not
					getTaiwanData(sensorData, date)
				else:
					getTaiwanEPAData(sensorData)

			if 'Airbox' in sourceList:
				getTaiwanAirboxData(sensorData, date, nwlat, selat, nwlng, selng)

			print("retrieved sensor data: ", sensorData)

			if len(sensorData['lat']) > 0:
				bbox = {
					'nwlat': float(nwlat),
					'selat': float(selat),
					'nwlng': float(nwlng),
					'selng': float(selng),
					'zoom': float(zoom)
				}

				#FIXME: change interp request according to current mode
				#interpData = ordinaryKrige(sensorData, bbox)
				k = [10]
				p = [5]

				if method == 'IDW':
					interpData = IDW(sourceList, sensorData, bbox, k, p)
				elif method == 'kriging':
					interpData = ordinaryKrige(sourceList, sensorData, bbox) #FIXME!
				else: #FIXME: should separate them, maybe use different request?
					interpData = None

				data = {
					'sensorData': sensorData,
					'interpData': interpData,
					'predictionMethod': method
				}



				# WRITE FILE HERE if desired
				"""
				filename = 'taiwan_epa_' + str(i) + '.txt'
				with open(filename, 'w') as outfile:
					json.dump(data, outfile, separators=(',', ':'), allow_nan = False, default=str)
				"""
			resp.body = json.dumps(data, separators=(',', ':'), allow_nan = False, default=str) #FIXME!

# for animating timesteps (for now, of the taiwan EPA data)
# NOTE: this is very much in beta mode!
class AnimationResource():
	def on_post(self, req, resp):
		requestBody = json.loads(req.stream.read().decode('utf-8'))
		index = requestBody.get('index')
		filename = 'taiwan/taiwan_epa_' + str(index) + '.txt'
		with open(filename) as json_file:
			data = json.load(json_file)
			resp.body = json.dumps(data, separators=(',', ':'), allow_nan = False, default=str)

# TODO: Load real-time data of Taiwan EPA.
# taiwan EPA historical data
def getTaiwanData(sensorData, date):
	path = '201803_Taichung_PM_(to_ISS).csv'
	date_string = parseDate('taiwanepa', date)
	print("date string: ", date_string)
	with open(path) as file:
		reader = csv.DictReader(file, delimiter=',')
		for row in reader:
			if row['PM2.5'] != '' and row['ID'][2:] == date_string:
				sensorData['source'].append('TaiwanEPA')
				sensorData['ID'].append(row['ID']) # is this even necessary
				sensorData['station'].append(row['location'])
				sensorData['lon'].append(Decimal(row['Lon']))
				sensorData['lat'].append(Decimal(row['Lat']))
				sensorData['pm25'].append(row['PM2.5']) #FIXME: can add more sophisticated info here
				sensorData['aqi'].append(aqi.to_iaqi(aqi.POLLUTANT_PM25, str(row['PM2.5']), algo=aqi.ALGO_EPA))

# NOTE: Real-time taiwan airbox data is unavailable for now.
# taiwan airbox historical data
def getTaiwanAirboxData(sensorData, date, nwlat, selat, nwlng, selng):
	date_string = parseDate("airbox", date)
	if "2018" in date_string:
		path = "Airboxes_match2018.csv"
	else:
		path = "Airboxes_match2019.csv"
	sensor_id = 0
	print("date for airbox data: ", date_string)
	with open(path) as file:
		reader = csv.DictReader(file, delimiter=',')
		for row in reader:
			if row[date_string] != 'NA':
				if float(row['lon']) > float(nwlng) and float(row['lon']) < float(selng) and float(row['lat']) > float(selat) and float(row['lat']) < float(nwlat):
					sensorData['source'].append('Airbox')
					sensorData['ID'].append(sensor_id)
					sensorData['lon'].append(Decimal(row['lon']))
					sensorData['lat'].append(Decimal(row['lat']))
					sensorData['pm25'].append(row[date_string])
					sensorData['aqi'].append(aqi.to_iaqi(aqi.POLLUTANT_PM25, str(row[date_string]), algo=aqi.ALGO_EPA))


# for purpleair --
# high-res data: get this only if there are fewer sensors than the threshold (currently, 10)
def getSensorData(sensorData):
	sensorIDs = sensorData['ID']

	if type(sensorIDs) is int:
		sensorIDs = [sensorIDs]

	sensorData = {key: [] for key in sensorData} #clear out the current (low-res) values FIXME: make sure this isn't clearing the AirNow values

	path = "https://www.purpleair.com/json?show="

	if type(sensorIDs) is list:
		length = len(sensorIDs)

	for ID in sensorIDs:
		with urlopen(path + str(ID)) as rawData:
			string = rawData.read().decode('utf-8')
			data = json.loads(string)['results'][0] #FIXME: this actually returns two results, one from each 'side' of the sensor...
			if data['DEVICE_LOCATIONTYPE'] == 'outside': #filter for OUTDOORS sensors only
				sensorData['source'].append('PurpleAir')
				sensorData['ID'].append(data['ID'])
				sensorData['lon'].append(data['Lon'])
				sensorData['lat'].append(data['Lat'])
				sensorData['pm25'].append(data['PM2_5Value']) #FIXME: can add more sophisticated info here
				sensorData['aqi'].append(aqi.to_iaqi(aqi.POLLUTANT_PM25, str(data['PM2_5Value']), algo=aqi.ALGO_EPA))

# also for purpleair --
# low-res (requires lon/lat bounds):
def getAreaData(sensorData, nwlat, selat, nwlng, selng):
	path =  "https://www.purpleair.com/data.json?fetch=true&nwlat=" + nwlat + "&selat=" + selat + "&nwlng=" + nwlng + "&selng=" + selng
	outsideIDs = getOutsideIDs()
	with urlopen(path) as rawData:
		string = rawData.read().decode('utf-8')

	result = json.loads(string, parse_float=Decimal)
	fields = result['fields']
	data = result['data']

	for row in data:
		if str(row[fields.index('pm')]) != 'None':
			if checkSensorOutside(row[fields.index('ID')], outsideIDs) == True:
				sensorData['source'].append('PurpleAir')
				sensorData['ID'].append(row[fields.index('ID')])
				sensorData['lon'].append(row[fields.index('Lon')])
				sensorData['lat'].append(row[fields.index('Lat')])
				value = row[fields.index('pm')]
				if value <= 500:
					sensorData['pm25'].append(value)
					sensorData['aqi'].append(aqi.to_iaqi(aqi.POLLUTANT_PM25, str(value), algo=aqi.ALGO_EPA))
				else:
					sensorData['pm25'].append(500)
					sensorData['aqi'].append(500)

def parseDate(option, date):
	# put the input date into the desired format

	print("parsing date: ", date)
	#FIXME: need to add the case of date+1!!! (i.e., the final hour of a day is requested)

	# incoming date format: YYYY-MM-DDThh:mm:ss.sssZ
	# return start date and end date
	if option == "thingspeak":
		# format needed for thingspeak:
		start_date = date[0:10] + "%20" + date[11:19]
		hour = str((int(date[11:13])+1)%24)
		if len(hour) == 1:
			hour = '0' + hour
		end_date = start_date[0:13] + hour + start_date[15:]
		return [start_date, end_date]

	elif option == "airnow":
		# format needed: YYYY-MM-DDThh:mm (i.e., need first 17 characters)
		start_date = date[0:16]
		# go one hour later --
		hour = str((int(date[11:13])+1)%24)
		if len(hour) == 1:
			hour = '0' + hour
		end_date = start_date[0:11] + hour + start_date[13:]
		return [start_date, end_date]

	elif option == 'airbox':
		# FIXME: this is only for displaying default airbox sensors
		if date != "":
			date_string = date[0:10] + ' ' + date[11:13]
		else:
			date_string = "2018-03-12 03"
		return date_string
	elif option == 'taiwanepa': #FIXME! the nights are actually staggered (see email)
		time_string = 'D'
		if date != "":
			hour = date[11:13]
			if int(hour) < 8 or int(hour) >= 20:
				time_string = 'N'
			date_string = date[2:4] + date[5:7] + date[8:10] + time_string
		else:
			date_string = "180312D"
		return date_string

def getThingSpeakData(sensorData, nwlat, selat, nwlng, selng, date):
	dates = parseDate("thingspeak", date)
	IDs = getThingspeakIDs()
	# for each ID in sensor data,
	# use IDs to look up thingspeak info,
	# look up, & replace pm25 and aqi values in sensordata
	getcontext().prec = 6
	rejected = []
	for i in range(len(sensorData['ID'])):
		try:
			channel = IDs[str(sensorData['ID'][i])]['thingspeakID']
			key = IDs[str(sensorData['ID'][i])]['key']
			url = 'https://thingspeak.com/channels/' + channel + '/feed.csv?api_key=' + key + '&start=' + dates[0] + "&end=" + dates[1]
			data = pd.read_csv(url, index_col=None)
			if data.empty == False:
				pm25 = data.field8
				value = pm25[0]
				if value <= 500:
					sensorData['pm25'][i] = Decimal(str(value)) #that's the value we want!
					sensorData['aqi'][i] = aqi.to_iaqi(aqi.POLLUTANT_PM25, str(value), algo=aqi.ALGO_EPA)
				else:
					sensorData['pm25'][i] = 500
					sensorData['aqi'][i] = 500
			else:
				rejected.append(i)
		except KeyError:
			rejected.append(i)

	for i in rejected[::-1]:
		for key, value in sensorData.items():
			del sensorData[key][i]

def getTaiwanEPAData(sensorData):
	# Taiwan EPA API: https://opendata.epa.gov.tw/DevelopZone/Sample/AQI/
	# format: https://opendata.epa.gov.tw/api/v1/{DataID}/?skip={skip}&top={top}&format={format}
	options = {}
	options["url"] = "https://opendata.epa.gov.tw/api/v1"
	options["DataID"] = "AQI"
	options["skip"] = "0" # skip # of data, a single day has 1238 in total
	options["top"] = "1000" # maximum is 1000
	options["format"] = "json"
	options["api_key"] = "+lt3fq2Ai0mZodNWv1BM9g"

	path = options["url"] \
		+ "/" + options["DataID"] \
		+ "/?skip=" + options["skip"] \
		+ "&top=" + options["top"] \
		+ "&format=" + options["format"] \
		+ "&token=" + options["api_key"]

	print("TaiwanEPA path: ", path)

	r = requests.get(path, verify=False)
	result = r.json()
	#FIXME: certificate verify failed error
	"""
	with urlopen(path) as rawData:
	    string = rawData.read().decode('utf-8')
	    result = json.loads(string, parse_float=Decimal)
	"""

	print(result)

	options['stations'] = ['彰化(大城)', '二林', '線西', '忠明', '大里', '沙鹿', '豐原', '彰化', '南投', '竹山', '埔里', '斗六']
	options['date'] = result[0]['PublishTime']
	for item in result:
	    if item['SiteName'] in options['stations'] :
	        sensorData['source'].append('TaiwanEPA')
	        sensorData['ID'].append(item['SiteId'])
	        sensorData['lon'].append(Decimal(item['Longitude']))
	        sensorData['lat'].append(Decimal(item['Latitude']))
	        sensorData['pm25'].append(item['PM2.5'])
	        sensorData['aqi'].append(item['AQI'])
	        sensorData['station'].append(item['SiteName'])



def getAirNowData(sensorData, nwlat, selat, nwlng, selng, date):
	# AirNow API: https://docs.airnowapi.org/Data/docs
	apiKey = "AF896438-7F72-4518-B975-3A10142F3FCE"
	options = {}
	options["url"] = "https://airnowapi.org/aq/data/"
	options["parameters"] = "pm25"
	options["bbox"] = nwlng + ',' + selat + ',' + selng + ',' + nwlat
	options["data_type"] = "b"
	options["format"] = "application/json"
	options["ext"] = "json"
	options["api_key"] = "AF896438-7F72-4518-B975-3A10142F3FCE"

	if date is not None and date != "":
		dates = parseDate("airnow", date)
		options["startdate"] = dates[0]
		options["enddate"] = dates[1]
		path = options["url"] \
			+ "?startdate=" + options["startdate"] \
			+ "&enddate=" + options["enddate"] \
            + "&parameters=" + options["parameters"] \
            + "&bbox=" + options["bbox"] \
            + "&datatype=" + options["data_type"] \
            + "&format=" + options["format"] \
            + "&api_key=" + options["api_key"]
	else:
		path = options["url"] \
	              + "?parameters=" + options["parameters"] \
	              + "&bbox=" + options["bbox"] \
	              + "&datatype=" + options["data_type"] \
	              + "&format=" + options["format"] \
	              + "&api_key=" + options["api_key"]

	print("airnow path: ", path)
	with urlopen(path) as rawData:
	    string = rawData.read().decode('utf-8')
	    result = json.loads(string, parse_float=Decimal)

	print("result: ", result)
	for item in result:
		if item['Value'] > 0.0:
			sensorData['source'].append('AirNow')
			sensorData['ID'].append(-1)
			sensorData['lon'].append(item['Longitude'])
			sensorData['lat'].append(item['Latitude'])
			sensorData['pm25'].append(item['Value'])
			sensorData['aqi'].append(aqi.to_iaqi(aqi.POLLUTANT_PM25, str(item['Value']), algo=aqi.ALGO_EPA))
