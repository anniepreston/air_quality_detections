# python imports
import os
from pdb import set_trace as bp

# pip imports
import falcon

# project imports
from airResources import AirSensorResource
from airResources import AnimationResource

# not super relevant
scriptPath = os.path.dirname(os.path.realpath(__file__))

app = application = falcon.API()

app.add_route('/air/detections', AirSensorResource()) # air quality data, real-time
app.add_route('/air/animation', AnimationResource())

# static file service (ideally we'd use something like nginx here but for ease of deployment this will suffice)
# serve up the index.html file
#app.add_route("/", StaticResource())

# serve up the static content resources
#app.add_route('/{filename}', StaticContentResource())

# this is sort of hacky... server script *has* to be ran from the ./backend directory
# on Mac, use this:
staticPath = '/'.join(__file__.split('/')[0:-2]) + '/frontend/'
# on Windows, use this:
# staticPath = '\\'.join(__file__.split('\\')[0:-2]) + "\\frontend\\"
# static file service
app.add_static_route('/', staticPath)
